import { ApiType, MultiSAMConfig, RequestHeaders, MLSApiConfig, SAMNode } from '../projects/multi-sam/src/shared/mls/config';
import { dump, load } from 'js-yaml';

const fs = require('fs');

console.log("--------------------------------------------------------------------------------------");
console.log("------ GENERATE CODEGEN YAML FOR TYPESCRIPT CODE- AND REMOTE SCHEMA-GENERATION  ------");
console.log("--------------------------------------------------------------------------------------");

const GRAPHQL_PATH: string = './graphql/';
const GENERATION_PATH: string = GRAPHQL_PATH + 'dist/';
const PROJECT_PATH: string = './projects/';
const PROJECT_TO_SAM_CONFIG_PATH: string = '/src/sam-config.json';
const CODEGEN_CONFIG_ROUTE: string ='./graphql/codegen.yml';


const TYPESCRIPT_MODEL_TYPES_PATH: string = GENERATION_PATH + 'model.types.d.ts';
const TYPESCRIPT_ENDPOINT_TYPES_PATH: string = GENERATION_PATH + 'endpoint.types.d.ts';
const TYPESCRIPT_QUERY_MODULES_PATH: string = GENERATION_PATH + 'query.modules.d.ts';
const TYPESCRIPT_SOURCE_MODULES_PATH: string = GENERATION_PATH + 'source.module.d.ts';
const BRIDGE_SCHEMA_AST_PATH: string = GENERATION_PATH + 'bridge.schema.graphql';
const ENDPOINT_SCHEMA_AST_PATH: string = GENERATION_PATH + 'endpoint.schema.graphql';

const GQL_SCHEMA_FILE_PATH: string = './**/*.schema.gql';
const GQL_DOCUMENT_FILE_PATH_FOR_QUERIES: string = './**/*.query.gql';
const GQL_DOCUMENT_FILE_PATH_FOR_SOURCES: string = './**/*.source.gql';

const TYPES_PLUGINS: string[] = [
  'typescript',
  'typescript-operations',
  'typescript-resolvers'
];
const TYPES_CONFIG: any = {
  skipTypename: true,
  addOperationExport: true,
  enumsAsTypes: true,
  declarationKind: {
    type: 'interface',
    resolver: 'interface'
  }
};
const MODULE_PLUGINS: string[] = [
  'typescript-graphql-files-modules'
];
const AST_PLUGIN: string[] = [
  'schema-ast'
];
const AST_CONFIG: any = {
  commentDescriptions: true
};

interface ApiRoute {
  uri: string
  type: ApiType | string
  header: RequestHeaders
}

function getSAMConfigs(): MultiSAMConfig[] {
  const samConfigs: MultiSAMConfig[] = [];
  fs.readdirSync(PROJECT_PATH, {withFileTypes: true})
    .filter(dir => dir.isDirectory())
    .forEach(dir => {
      try {
        samConfigs.push(JSON.parse(fs.readFileSync(PROJECT_PATH + dir.name + PROJECT_TO_SAM_CONFIG_PATH)));
      } catch (e) {
        if (e.code !== 'ENOENT') {
          console.error(e);
        }
      }
    });
  return samConfigs;
}

function getBridgeWebAPIs(samConfigs: MultiSAMConfig[]): ApiRoute[] {
  const gqlURIs: ApiRoute[] = [];
  samConfigs.forEach(config => {
    for (const nodeKey of Object.keys(config.nodes)) {
      const node: SAMNode = config.nodes[nodeKey];
      if (!node.gateway || !node.gateway.externalApi) {
        continue;
      }
      gqlURIs.push(retrieveApiRoutes(node.gateway));
    }
  });
  return gqlURIs;
}

function getEndpointWebAPIs(samConfigs: MultiSAMConfig[]): ApiRoute[] {
  const gqlURIs: ApiRoute[] = [];
  samConfigs.forEach(config => {
    for (const nodeKey of Object.keys(config.nodes)) {
      const node: SAMNode = config.nodes[nodeKey];
      if (!node.endpoints || node.endpoints.length === 0) {
        continue;
      }
      for (const end of node.endpoints) {
        if (end.type !== 'WebApi') {
          continue;
        }
        gqlURIs.push(retrieveApiRoutes(end.dataSource as MLSApiConfig));
      }
    }
  });
  return gqlURIs;
}

function retrieveApiRoutes(api: MLSApiConfig): ApiRoute {
  if (api.type === 'GQL') {
    let uri: string = (api.https) ? 'https://' : 'http://';
    uri += api.host + api.route;
    console.log('-----> INJECT GQL URI: ' + uri);
    return {
      uri: uri,
      type: 'GQL',
      header: api.header
    };
  }
  return undefined;
}

function generateSchemaYMLObject(remoteGQLAPIs: ApiRoute[]): any[] {
  const globalSchemes: any[] = [];
  if (!remoteGQLAPIs) {
    return globalSchemes;
  }
  remoteGQLAPIs.forEach(api => {
    while (true) {
      const duplicateIndex: number = globalSchemes.findIndex(uri => {
        if (typeof uri === 'string') {
          return uri === api.uri;
        } else {
          return !!uri[api.uri];
        }
      });
      if (duplicateIndex > -1) {
        globalSchemes.splice(duplicateIndex, 1);
      } else {
        break;
      }
    }
    if (api.header) {
      let schemaObj: any = {};
      schemaObj[api.uri] = {
        headers: api.header
      };
      globalSchemes.push(schemaObj);
    } else {
      globalSchemes.push(api.uri);
    }
  });
  return globalSchemes;
}

function generateTypeYMLObject(schemas: any[], docs: any[] = []): Object {
  const obj: Object = {
    schema: schemas,
    plugins: TYPES_PLUGINS,
    config: TYPES_CONFIG
  };
  if (docs && docs.length > 0) {
    obj['documents'] = docs;
  }
  return obj;
}

function generateModuleYMLObject(schemas: any[], docs: any[] = []): Object {
  const obj: Object = {
    schema: schemas,
    plugins: MODULE_PLUGINS,
  };
  if (docs && docs.length > 0) {
    obj['documents'] = docs;
  }
  return obj;
}

function generateAstYMLObject(schema: any[]): Object {
  return {
    schema: schema,
    plugins: AST_PLUGIN,
    config: AST_CONFIG
  }
}

function generateCodegenYMLObject(): any {
  const samConfigs: MultiSAMConfig[] = getSAMConfigs();
  const bridgeApis: ApiRoute[] = getBridgeWebAPIs(samConfigs);
  const bridgeSchemas: any[] = generateSchemaYMLObject(bridgeApis);
  const endpointApis: ApiRoute[] = getEndpointWebAPIs(samConfigs);
  const endPointSchemas: any[] = generateSchemaYMLObject(endpointApis);
  const codegen: any = {};
  codegen.generates = {};
  codegen.generates[TYPESCRIPT_MODEL_TYPES_PATH] = generateTypeYMLObject([GQL_SCHEMA_FILE_PATH], [GQL_DOCUMENT_FILE_PATH_FOR_QUERIES]);
  codegen.generates[TYPESCRIPT_QUERY_MODULES_PATH] =  generateModuleYMLObject([GQL_SCHEMA_FILE_PATH], [GQL_DOCUMENT_FILE_PATH_FOR_QUERIES]);
  if (bridgeApis.length > 0) {
    codegen.generates[BRIDGE_SCHEMA_AST_PATH] = generateAstYMLObject(bridgeSchemas);
  }
  if (endpointApis.length > 0) {
    codegen.generates[ENDPOINT_SCHEMA_AST_PATH] = generateAstYMLObject(endPointSchemas);
    codegen.generates[TYPESCRIPT_ENDPOINT_TYPES_PATH] = generateTypeYMLObject(endPointSchemas, [GQL_DOCUMENT_FILE_PATH_FOR_SOURCES]);
  }
  const sourceSchemas: any[] = generateSchemaYMLObject(endpointApis.concat(bridgeApis));
  if (sourceSchemas.length > 0) {
    codegen.generates[TYPESCRIPT_SOURCE_MODULES_PATH] = generateModuleYMLObject(sourceSchemas, [GQL_DOCUMENT_FILE_PATH_FOR_SOURCES]);
  }
  return codegen;
}

try {
  fs.writeFileSync(CODEGEN_CONFIG_ROUTE, dump(generateCodegenYMLObject(), {forceQuotes: true}));
} catch (e) {
  console.error(e);
}

// TODO: Change retrieving data from sam config to getter function of configurator. Getter function of configurator can type check
//  the SAM-Config to better report Config Errors!

console.log("-----> << FINISHED >>  ");
console.log("--------------------------------------------------------------------------------------");



