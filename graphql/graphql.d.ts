/**
 * This file declares graphql files, so they be recognized by typescript.
 * There are 2 ways of exporting them:
 *    - as raw string
 *    - as DocumentNode Object from graphql library
 */


// declare module '*.graphql' {
//   const schema: string;
//
//   export = schema;
// }

declare module '*.schema.gql' {
  import { DocumentNode } from 'graphql';
  const schema: DocumentNode;

  export = schema;
}

