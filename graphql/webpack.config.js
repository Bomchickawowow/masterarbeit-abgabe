/**
 * Add webpack handling for graphql files, so they will be recognized as javascript import
 * There are 2 ways of defining them:
 *      - as a raw string by 'raw-loader'
 *      - as a DocumentNode by 'graphql-tag/loader'
 */


module.exports = {
  module: {
    rules: [
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      }
    ]
  }
};
