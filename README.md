# Masterarbeit

Referenzimplementierung zur Masterarbeit **"Robuste Webanwendungen in volatilen Netzwerken mithilfe von 
Multi-Layered SAM-Pattern als hoch-sklaierbare Softwarearchitektur"**
    
## Benutzung
> Für die Benutzung wird Node.js mit NPM benötigt!
(Dafür online einfach Node.js herunterladen und installieren)


Vor der Benutzung müssen erst alle NPM Pakete installiert werden. 
Dafür mit der Kommandozeile zum Root-Verzeichnis des Projekts navigieren und folgendes eingeben:

```
npm install
```

Danach müssen die Datenstrukturen bezüglich der Codegenerierung mit folgenden Befehl erstellt werden:

```
npm run codegen
```

Um schließlich den SSR Dev-Server ("Server-side rendering Development Server") zu starten, 
einfach folgenden Befehl eingeben:

```
npm run dev:ssr
```

Nun kann die Anwendung lokal im Browser über die Adresse. ``http://localhost:4200/`` aufgerufen werden.

## Überblick

Dieses Projekt basiert auf den Konzepten der Masterarbeit und verwendet als Kerntechnologien Angular Universal, GraphQL Codegen, Apollo Server & Client und Immer.js.
Neben den in der Masterarbeit genannten Technologien, wird auch viel mit DependencyInjection gearbeitet, um im Code leichter Zugang zu den benötigten Klassen zu erlangen.

Unter ``graphql`` können die Dateien zur Einbettung von GraphQL in Typescript und zur Codegenerierung begutachtet werden.
Dabei beschreibt die ```config.generate.ts``` wie aus der Konfigurationsdatei die externen GraphQL Schemata für die Codegenerierung erlangt werden können.
Die Datei ``codegen.yml`` gibt danach für GraphQL Codegen die Instruktionen vor.
Unter ``grapql/dist`` landen dann die generierten Datenstrukturen.

Unter ``projects/multi-sam/src`` findet sich dann die Projektdatein zur Architektur und Beispiel Anwendung.
Dabei findet man unter ``project/multi-sam/src/shared/mls`` alle Definition zu der Architektur ("mls" steht für "Multi-Layered SAM")
Zusätzlich stehen unter ``project/multi-sam/src/shared/mls-modules/todo`` ist das Anwendungsmodul zur TodoListe mit allen seinen Komponente aufgelistet.
Im Verzeichnis ``projects/multi-sam/src/server`` sind die Server-eignene Architektur Dateien
Schließlich sind unter ``projects/multi-sam/src/app`` die Client-Dateien mit dem gesamten Angular View-Elementen.

## Anmerkung

Auch wenn durchweg versucht wurde, die Anwendungsimplementierung mit der Architektur simple und leicht verständlich zu halten, ist die Verwendung der Anwendungsmodule durch die Zeit recht unübersichtlich geworden.
Zwar konnte im Rahmen der Masterarbeit die meisten Designziele umgesetzt werden, aber aus persönlichen Interesse werde ich diese Architektur weiterentwicklen
bis sie ein Stand hat, der es auch für Laien einfach macht, damit eine Anwendung zu entwickeln.

---
Da im Laufe der Arbeit die Namen der Komponenten öfters gewechselt wurden, kann es sein, dass in den Kommentaren teilweise veraltete Namen verwendet wurden.

