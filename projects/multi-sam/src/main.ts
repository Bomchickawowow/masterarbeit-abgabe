import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Logger } from './shared/util/logger';

if (environment.production) {
  enableProdMode();
}

document.addEventListener('DOMContentLoaded', () => {
  Logger.initSource('client');
  platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
});
