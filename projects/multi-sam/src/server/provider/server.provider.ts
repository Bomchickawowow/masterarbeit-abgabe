import { Provider } from '@angular/core';
import { TodoReducerCollection } from '../specs/todo.spec';

export const serverProvider: Provider[] = [
  TodoReducerCollection
];
