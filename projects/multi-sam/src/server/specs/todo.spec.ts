import { ActionCollection, ActionSpec } from '../../shared/mls/action';
import { Model } from '../../shared/mls/model';
import { Injectable } from '@angular/core';
import { MutationAddTodoArgs, MutationFinishTodoArgs, MutationRemoveTodoArgs, MutationUnfinishTodoArgs } from 'graphql/dist/model.types';
import { TodoAction } from '../../shared/mls-modules/todo/todo.actions';

@Injectable()
export class TodoReducerCollection implements ActionCollection {
  executionTarget: 'server';
  specs: ActionSpec<TodoAction>[] = [
    {type: 'ADD-TODO', reducer: this.reduceAddTodo},
    {type: 'REMOVE-TODO', reducer: this.reduceDeleteTodo},
    {type: 'FINISH-TODO', reducer: this.reduceFinishTodo},
    {type: 'UNFINISH-TODO', reducer: this.reduceUnfinishTodo}
  ];

  private reduceAddTodo(model: Model, payload: MutationAddTodoArgs): void {
    if (model.todo.todoList.map(todo => todo.text).includes(payload.todo)) {
      throw new Error(`Could not create Todo Item. The Name "${payload}" of Todo Item already exists!`);
    }
    model.todo.todoList.push({text: payload.todo, done: false});
  }

  private reduceDeleteTodo(model: Model, payload: MutationRemoveTodoArgs): void {
    if (!model.todo.todoList.map(todo => todo.text).includes(payload.todo)) {
      throw new Error(`Could not delete Todo Item. Todo Item "${payload}" does not exist!`);
    }
    const index: number = model.todo.todoList.findIndex(todo => todo.text === payload.todo);
    model.todo.todoList.splice(index, 1);
  }

  private reduceFinishTodo(model: Model, payload: MutationFinishTodoArgs): void {
    if (!model.todo.todoList.map(todo => todo.text).includes(payload.todo)) {
      throw new Error(`Could not finish Todo Item. Todo Item "${payload}" does not exist!`);
    }
    const index: number = model.todo.todoList.findIndex(todo => todo.text === payload.todo);
    if (model.todo.todoList[index].done) {
      throw new Error(`Could not finish Todo Item. Todo Item "${payload}" is already finished!`);
    }
    model.todo.todoList[index].done = true;
  }

  private reduceUnfinishTodo(model: Model, payload: MutationUnfinishTodoArgs): void {
    if (!model.todo.todoList.map(todo => todo.text).includes(payload.todo)) {
      throw new Error(`Could not finish Todo Item. Todo Item "${payload}" does not exist!`);
    }
    const index: number = model.todo.todoList.findIndex(todo => todo.text === payload.todo);
    if (!model.todo.todoList[index].done) {
      throw new Error(`Could not finish Todo Item. Todo Item "${payload}" is already unfinished!`);
    }
    model.todo.todoList[index].done = false;
  }
}
