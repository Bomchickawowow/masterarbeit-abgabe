import { Type } from '@angular/core';
import { TodoModule } from './todo/todo.module';
import { MLSModule } from '../mls/mls-module';
import { todoProvider } from './todo/todo.provider';

export const sharedModuleProvider: Type<MLSModule>[] = [
  TodoModule
];

export const sharedProvider: Type<any>[] = [
  ...todoProvider
];

// export const sharedDispenser: Type<Dispatcher>[] = [
//   TodoDispatcher
// ];
