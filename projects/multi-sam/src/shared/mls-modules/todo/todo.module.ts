import { Injectable, Optional } from '@angular/core';
import { MLSModule } from '../../mls/mls-module';
import { TodoDispatcher } from './todo.dispatcher';
import { TodoSelections } from './todo.selections';
import { TodoActions } from './todo.actions';
import { Store } from '../../mls/store';

@Injectable()
export class TodoModule extends MLSModule {
  constructor(
    public selections: TodoSelections,
    public actions: TodoActions,
    public dispatcher: TodoDispatcher,
    public store: Store
  ) {
    super(selections, actions, dispatcher, store);
    this.register();
  }
}

