import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TodoState } from '../../../app/todo/todo.state';
import { ActionFactory } from '../../mls/action';
import { ExecutionState, Store } from '../../mls/store';
import { Injectable } from '@angular/core';
import { Dispatcher, GQLResolution } from '../../mls/resolver';
import { TodoModel } from 'graphql/dist/model.types';
import { DocumentNode } from 'graphql';
import TodoSchema from './todo.schema.gql';
import { TodoAction, TodoActions } from './todo.actions';
import { TodoSelections } from './todo.selections';

@Injectable()
export class TodoDispatcher extends Dispatcher {
  schema: DocumentNode = TodoSchema;
  resolution: GQLResolution = {
    Query: {
      todo: (parent, args, context, info) => this.getTodoModel()
    },
    Mutation: {
      addTodo: (parent, args) => this.addTodoItem(args.todo),
      removeTodo: (parent, args) => this.removeTodoItem(args.todo),
      finishTodo: (parent, args) => this.finishTodoItem(args.todo),
      unfinishTodo: (parent, args) => this.unfinishTodoItem(args.todo)
    }
  };

  constructor(
    protected store: Store,
    protected af: ActionFactory,
    protected selections: TodoSelections,
    protected actions: TodoActions
  ) {
    super(store, af, selections, actions);
  }

  getTodoModel(): Observable<TodoModel> {
    return this.store.getState(this.selections.specs.map(s => s.selection)).pipe(map(model => {
      return model.todo;
    }));
  }

  getTodoAsLists(): Observable<TodoState> {
    return this.getTodoModel().pipe(map(model => {
      const state: TodoState = {finishedTodos: [], unfinishedTodos: []};
      for (const item of model.todoList) {
        if (item.done) {
          state.finishedTodos.push(item.text);
        } else {
          state.unfinishedTodos.push(item.text);
        }
      }
      return state;
    }));
  }

  addTodoItem(todoText: string): Observable<any> {
    return this.store.present(this.af.create<TodoAction>('ADD-TODO', {todo: todoText}));
  }

  removeTodoItem(todoText: string): Observable<any> {
    return this.store.present(this.af.create<TodoAction>('REMOVE-TODO', {todo: todoText}));
  }

  finishTodoItem(todo: string): Observable<any> {
    return this.store.present(this.af.create<TodoAction>('FINISH-TODO', {todo: todo}));
  }

  unfinishTodoItem(todo: string): Observable<any> {
    return this.store.present(this.af.create<TodoAction>('UNFINISH-TODO', {todo: todo}));
  }
}
