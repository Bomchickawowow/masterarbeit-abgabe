import { SelectionBundle, SelectionSpec } from '../../mls/selection';
import { Todo } from './todo.query.gql';
import { Injectable } from '@angular/core';

@Injectable()
export class TodoSelections implements SelectionBundle {
  specs: SelectionSpec[] = [
    {
      selection: ['todo'],
      node: 'server',
      query: Todo
    }
  ];
}
