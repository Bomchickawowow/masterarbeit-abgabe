import { Type } from '@angular/core';
import { TodoModule } from './todo.module';
import { TodoSelections } from './todo.selections';
import { TodoActions } from './todo.actions';
import { TodoDispatcher } from './todo.dispatcher';

export const todoProvider: Type<any>[] = [
  TodoSelections,
  TodoActions,
  TodoDispatcher,
  TodoModule
];
