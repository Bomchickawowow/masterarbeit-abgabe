import { Action, ActionBundle, ActionCollection, mergeActionCollections } from '../../mls/action';
import { AddTodo, FinishTodo, RemoveTodo, UnfinishTodo } from './todo.query.gql';
import {
  MutationAddTodoArgs,
  MutationFinishTodoArgs,
  MutationRemoveTodoArgs,
  MutationUnfinishTodoArgs
} from 'graphql/dist/model.types';
import { Injectable, Optional } from '@angular/core';
import { TodoReducerCollection } from '../../../server/specs/todo.spec';

@Injectable()
export class TodoActions implements ActionBundle<TodoAction> {
  collections: ActionCollection<TodoAction>[] =  [
    {
      executionTarget: 'server',
      specs: [
        {type: 'ADD-TODO', mutation: AddTodo},
        {type: 'REMOVE-TODO',  mutation: RemoveTodo},
        {type: 'FINISH-TODO',  mutation: FinishTodo},
        {type: 'UNFINISH-TODO', mutation: UnfinishTodo}
      ]
    }
  ];

  // This way you can inject reducer logic or other bundle specific stuff, that should not be available on other nodes!
  // If nothing needs to be injected, you can simply insert it all into the actions property
  constructor(@Optional() injectedCollection: TodoReducerCollection) {
    if (injectedCollection) {
      this.collections[0] = mergeActionCollections(this.collections[0], injectedCollection) as ActionCollection<TodoAction>;
    }
  }
}

export type TodoActionType = 'ADD-TODO' | 'REMOVE-TODO' | 'FINISH-TODO' | 'UNFINISH-TODO';
export interface TodoAction extends Action {
  type: TodoActionType;
  payload: MutationAddTodoArgs | MutationRemoveTodoArgs | MutationFinishTodoArgs | MutationUnfinishTodoArgs;
}
