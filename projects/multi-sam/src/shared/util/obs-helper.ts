import { bindCallback, Observable } from 'rxjs';

export abstract class ObservableHelper {

  public static fromCallback<T>(funcLoc: Object, func: CallBackFunc<T>, ...args: any[]): Observable<T> {
    const obsFunc: (...args: any[]) => Observable<T> = bindCallback(func);
    return obsFunc.call(funcLoc, args);
  }
}

export type CallBackFunc<T> = (callback: (data: T) => void, ...args: any[]) => void;
