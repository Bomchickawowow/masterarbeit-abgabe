import { ObjectHelper } from './object-helper';

export abstract class Path {
  public static isSubPath(part: any[], whole: any[]): boolean {
    if (!part) {
      // EMPTY SET IS ALWAYS SUBSET
      return true;
    }
    if (!whole) {
      // NON-EMPTY SET IS NEVER SUBSET OF EMPTY SET
      return false;
    }
    if (part.length > whole.length) {
      return false;
    }
    for (let i: number = 0; i < part.length; i++) {
      if (part[i] !== whole[i]) {
        return false;
      }
    }
    return true;
  }

  public static isRelated(path1: any[], path2: any[]): boolean {
    if (!path1 || !path2) {
      // EMPTY SET IS ALWAYS RELATED
      return true;
    }
    let part: any[] = path1;
    let whole: any[] = path2;
    if (part.length > whole.length) {
      part = whole;
      whole = path1;
    }
    return Path.isSubPath(part, whole);
  }

  public static areRelated(multiPath: any[][] | IterableIterator<any[]>, path: any[]): boolean {
    for (const arr of multiPath) {
      if (Path.isRelated(arr, path)) {
        return true;
      }
    }
    return false;
  }

  public static hasDuplicates(arr: any[]): boolean {
    if (arr.length < 2) {
      return false;
    }
    for (let i: number = 0; i < arr.length; i++) {
      for (let j: number = i + 1; j < arr.length; j++) {
        if (arr[i] === arr[j]) {
          return true;
        }
      }
      if (i === arr.length - 2) {
        break;
      }
    }
    return false;
  }

  public static longestCommonPath(multiPaths: any[][]): any[] {
    if (!multiPaths) {
      return [];
    }
    let shortest: any[] = multiPaths[0];
    for (const p of multiPaths) {
      if (p.length < shortest.length) {
        shortest = p;
      }
    }
    const common: any[] = [];
    for (let i: number = 0; i < shortest.length; i++) {
      const ele: any = shortest[i];
      for (const p of multiPaths) {
        if (ele !== p[i]) {
          return common;
        }
      }
      common.push(ele);
    }
    return common;
  }

  public static travel(obj: any, path: string[]): any {
    let sub: any = obj;
    let traveled: string = '';
    for (const r of path) {
      if (ObjectHelper.hasOwnProperty(sub, r)) {
        sub = sub[r];
        traveled = `${traveled}${r}.`;
      } else {
        throw Error(`Cannot travel path of given object! The route "${traveled}${r}" does not exist on object!`);
      }
    }
    return sub;
  }

  public static toString(path: string[]): string {
    return path.reduce((p, c) => `${p}.${c}`);
  }
}
