export class Logger {
  private static SHOW_DEBUG: boolean = true;
  private static SHOW_INFO: boolean = true;
  private static SHOW_WARNING: boolean = true;
  private static SHOW_ERROR: boolean = true;

  private static CONSOLE_RESET_COLOR: string = '\x1b[0m';
  private static CONSOLE_RED: string = '\x1b[31m';
  private static CONSOLE_YELLOW: string = '\x1b[33m';
  private static CONSOLE_GREEN: string = '\x1b[32m';
  private static CONSOLE_BLUE: string = '\x1b[34m';
  private static CONSOLE_CYAN: string = '\x1b[36m';

  private static ONE_TAB_LENGTH: number = 31;
  private static TWO_TAB_LENGTH: number = Logger.ONE_TAB_LENGTH - 8;
  private static THREE_TAB_LENGTH: number = Logger.TWO_TAB_LENGTH - 8;
  private static FOUR_TAB_LENGTH: number = Logger.THREE_TAB_LENGTH - 8;

  private static SOURCE: string;

  public static initSource(source: string): void {
    if (Logger.SOURCE) {
      return;
    } else {
      Logger.SOURCE = source.substr(0, 2).toUpperCase();
    }
  }

  public static debug(info: any, location?: any): void {
    if (Logger.SHOW_DEBUG === true) {
      console.log('', Logger.addTabChars(Logger.getMetaInformation(location)), info, '');
    }
  }

  public static info(info: any, location?: any): void {
    if (Logger.SHOW_INFO === true) {
      console.log(Logger.CONSOLE_CYAN, Logger.addTabChars(Logger.getMetaInformation(location)), info, Logger.CONSOLE_RESET_COLOR);
    }
  }

  public static warn(info: any, location?: any): void {
    const meta: string = Logger.getMetaInformation(location);
    if (Logger.SHOW_WARNING === true) {
      console.warn(Logger.CONSOLE_YELLOW, Logger.addTabChars(meta),
        (info instanceof Error) ?  Logger.adjustErrorObject(info, meta.length) : info, Logger.CONSOLE_RESET_COLOR);
    }
  }

  public static error(info: any, location?: any): void {
    const meta: string = Logger.getMetaInformation(location);
    if (Logger.SHOW_ERROR === true) {
      console.error(Logger.CONSOLE_RED, Logger.addTabChars(meta),
        (info instanceof Error) ?  Logger.adjustErrorObject(info, meta.length) : info, Logger.CONSOLE_RESET_COLOR);
    }
  }

  private static getMetaInformation(location?: any): string {
    return Logger.getTimeText() + Logger.getLocationText(location);
  }

  private static getLocationText(location?: any): string {
    const source: string = (Logger.SOURCE) ? `[${Logger.SOURCE}]` : '';
    const loc: string = (location) ? `[${location.constructor.name}]` : '';
    return `${source}${loc}: `;
  }

  private static getTimeText(): string {
    const time: Date = new Date();
    return `[${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}-${time.getMilliseconds()}]`;
  }

  private static addTabChars(mes: string ): string {
    return mes + Logger.getTabChars(mes.length);
  }

  private static getTabChars(length: number): string {
    if (length >= Logger.ONE_TAB_LENGTH) {
      return '\t';
    } else if (length >= Logger.TWO_TAB_LENGTH) {
      return '\t\t';
    } else if (length >= Logger.THREE_TAB_LENGTH) {
      return '\t\t\t';
    } else if (length >= Logger.FOUR_TAB_LENGTH) {
      return '\t\t\t\t';
    }
    return '\t\t\t\t\t';
  }

  private static adjustErrorObject(err: Error, metaSize: number): string {
    const gap: string = ' '.repeat(metaSize) + Logger.getTabChars(metaSize);
    let output: string = err.name + ':\n';
    const lines: string[] = err.message.split('\n');
    for (const line of lines) {
      output += `${gap}   ${line}\n`;
    }
    return output.slice(0, -1);
  }
}
