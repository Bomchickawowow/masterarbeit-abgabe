export abstract class ObjectHelper {

  public static hasOwnProperty(obj: Object, prop: string): boolean {
    if (obj.hasOwnProperty) {
      return obj.hasOwnProperty(prop);
    } else {
      return (Object.keys(obj).findIndex(k => k === prop) > -1);
    }
  }

  public static hasOwnProperties(obj: Object): boolean {
    return Object.keys(obj).length > 0;
  }

  public static getOwnProperties(obj: Object): any[] {
    return Object.values(obj);
  }
}
