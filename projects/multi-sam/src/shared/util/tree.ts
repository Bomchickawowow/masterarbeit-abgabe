import { Path } from './path';

export abstract class RecursiveTree {
  public static depthFirstSearch<T = any>(tree: T, routeToChildren: string[], searchFn: (e: T) => boolean): T {
    if (searchFn(tree)) {
      return tree;
    }
    try {
      const children: T[] = Path.travel(tree, routeToChildren);
      for (const child of children) {
        const foundElement: T = RecursiveTree.depthFirstSearch<T>(child, routeToChildren, searchFn);
        if (foundElement) {
          return foundElement;
        }
      }
      return undefined;
    } catch (e) {
      return undefined;
    }
  }
}
