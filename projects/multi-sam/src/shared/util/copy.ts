export abstract class Copy {
  /**
   * Only copys first level properties!
   *
   * @param obj Object to copy
   */
  public static shallow(obj: Object): Object {
    return Object.assign({}, obj);
  }

  /**
   * Fast Deep Copy with potential data loss.
   * WARNING: Cannot copy nested Dates, functions, undefined, Inifinity, RegExps, Maps, Sets, Blobs, FileLists, ImageDatas,
   *          sparse Arrays, Typed Arrays or other complex types!
   *
   * @param obj Object to copy
   */
  public static naiveDeep(obj: Object): Object {
    return JSON.parse(JSON.stringify(obj));
  }

  /**
   * Slow deep copy with no data loss.
   *
   * @param obj Object to copy
   */
  public static trueDeep(obj: Object): Object {
    // TODO: embed library function (Immutable.js ?!) for real deep copy!
    return Copy.naiveDeep(obj);
  }
}
