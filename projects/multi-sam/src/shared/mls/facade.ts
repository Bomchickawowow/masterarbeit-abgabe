import { bindCallback, Observable } from 'rxjs';
import { DocumentNode } from 'graphql';
import { ApolloClient } from '@apollo/client/core';
import { GQLObject } from './selection';
import { Logger } from '../util/logger';

export class Facade {
  constructor(
    private dataSources: Map<string, DataSourceHandler>
  ) {}

  from(sourceId: string): DataSourceHandler {
    return this.dataSources.get(sourceId);
  }
}

export interface DataSourceHandler {
  // create(...data: any[]): Observable<any>;
  // read(...data: any[]): Observable<any>;
  // update(...data: any[]): Observable<any>;
  // delete(...data: any[]): Observable<any>;
}

export class GQLSource implements DataSourceHandler {
  constructor(
    protected apollo: ApolloClient<any>,
    protected endpointName: string
  ) {}

  query(gqlQuery: DocumentNode, variables: any = {}): Observable<any> {
    const retrieveModelObs: (q: GQLObject, v: any) => Observable<any> = bindCallback(this.sendQuery);
    return retrieveModelObs.call(this, gqlQuery, variables);
  }

  mutate(gqlMutation: DocumentNode, variables: any = {}): Observable<any> {
    const sendActionObs: (m: GQLObject, v: any) => Observable<void> = bindCallback(this.sendActionMutation);
    return sendActionObs.call(this, gqlMutation, variables);
  }

  protected sendQuery(query: GQLObject, variables: any = {}, callback: (data: any) => void): void {
    const context: any = {};
    context.location = this.endpointName;
    this.apollo.query({
      query: query,
      context: context,
      variables: variables
    }).then(data => {
      callback(data.data);
    }).catch(err => {
      Logger.error(err, this);
    });
  }

  protected sendActionMutation(mutation: GQLObject, variables: any = {}, callback: (data: any) => void): void {
    const context: any = {};
    context.location = this.endpointName;
    this.apollo.mutate({
      mutation: mutation,
      context: context,
      variables: variables
    }).then(data => {
      callback(data);
    }).catch(err => {
      Logger.error(err, this);
    });
  }
}

export interface RESTSource extends DataSourceHandler {

}

export interface DatabaseSource extends DataSourceHandler {

}
