import { MultiSAMConfig, RequestHeaders, MLSApiConfig, SAMNode } from './config';
import { DocumentNode } from 'graphql';
import { Logger } from '../util/logger';
import { GraphQLDate, GraphQLTime, GraphQLDateTime } from 'graphql-iso-date';
import GraphQLJSON from 'graphql-type-json';
import { Dispatcher, SAMGQLContext, MLSResolver, Resourcer } from './resolver';
import { Observable } from 'rxjs';
import { ExecutionState } from './store';
import { map, take } from 'rxjs/operators';
import { Resolvers } from 'graphql/dist/model.types';
import RootSchema from './gql/root.schema.gql';
import { v4 } from 'uuid';
import { ApolloClient } from '@apollo/client/core';
import { DataSourceHandler, GQLSource } from './facade';


export class Configurator {
  private readonly clientId: string;

  constructor(
    private readonly config: MultiSAMConfig
  ) {
    if (!this.config.nodes || !this.config.default) {
      throw new Error('Given SAM Config File is invalid! Look at config.d.ts for proper config structure!');
    }
    this.clientId = v4();
  }

  getClientId(): string {
    return this.clientId;
  }

  getServerAPIConfig(resolvers: MLSResolver[]): ApolloGQLConfig {
    return this.getApolloGQLConfig(resolvers.filter(r => r instanceof  Dispatcher));
  }

  getRepresentationBorderConfig(resolvers: MLSResolver[]): ApolloGQLConfig {
    return this.getApolloGQLConfig(resolvers.filter(r => r instanceof Resourcer));
  }

  getApolloGQLConfig(resolvers: MLSResolver[]): ApolloGQLConfig {
    if (!resolvers || resolvers.length === 0) {
      return undefined;
    }
    const config: ApolloGQLConfig = {
      typeDefs: [
        RootSchema
      ],
      resolvers: {
        Date: GraphQLDate,
        Time: GraphQLTime,
        DateTime: GraphQLDateTime,
        JSON: GraphQLJSON,
        Query: {},
        Mutation: {}
      },
      subscriptions: {}
    };
    for (const r of resolvers) {
      if (r.schema) {
        if (config.typeDefs.map(s => this.getGQLSchemaString(s)).findIndex(s => s === this.getGQLSchemaString(r.schema)) === -1) {
          config.typeDefs.push(r.schema);
        } else {
          Logger.warn('A schema is duplicated in SAMResolvers!', this);
        }
      }
      if (!r.resolution) {
        continue;
      }
      if (r.resolution.Query) {
        for (const k of Object.keys(r.resolution.Query)) {
          if (config.resolvers.Query.hasOwnProperty(k)) {
            throw new Error(`Failed to merge GQL Resolver of Gateway! The key "${k}" in the Query Resolver
            exists multiple times in different SAMResolvers`);
          }
          config.resolvers.Query[k] = (p, a, c, i) => this.toResolvedPromise(r.resolution.Query[k](p, a, c, i));
        }
      }
      if (r.resolution.Mutation) {
        // TODO: get Action Meta information from Client and forward it!
        for (const k of Object.keys(r.resolution.Mutation)) {
          if (config.resolvers.Mutation.hasOwnProperty(k)) {
            throw new Error(`Failed to merge GQL Resolver of Gateway! The key "${k}" in the
            Mutation Resolver exists multiple times in different SAMResolvers`);
          }
          config.resolvers.Mutation[k] = (p, a, c, i) => this.toResolvedPromise(r.resolution.Mutation[k](p, a, c, i));
        }
      }
    }
    return config;
  }

  setupWebDataSources(apollo: ApolloClient<any>, endPointNames: string[]): Map<string, DataSourceHandler> {
    const sources: Map<string, DataSourceHandler> = new Map<string, DataSourceHandler>();
    if (!endPointNames || endPointNames.length === 0) {
      return sources;
    }
    // TODO: Differentiate between GQL- and REST-Endpoint !
    for (const end of endPointNames) {
      sources.set(end, new GQLSource(apollo, end));
    }
    return sources;
  }

  getGatewayConfig(nodeName: string): ServerAPIConfig {
    try {
      const server: SAMNode = this.config.nodes[nodeName];
      this.checkConfigServerNode(nodeName, server);
      return new ServerAPIConfig(server.gateway.https, server.gateway.host, server.gateway.route, server.gateway.type, server.gateway.port);
    } catch (e: any) {
      const error: Error = e as Error;
      throw new SAMConfigError(`Failed to retrieve Gateway Config of node "${nodeName}"! ${error.message}`);
    }
  }

  getBridgePoints(nodeName: string): WebPoint[] {
    try {
      const client: SAMNode = this.config.nodes[nodeName];
      this.checkConfigClientNode(nodeName, client);
      const points: WebPoint[] = [];
      if (!client.bridges) {
        return points;
      }
      for (const con of client.bridges) {
        const node: SAMNode = this.config.nodes[con.node];
        this.checkConfigServerNode(con.node, node, false);
        const api: MLSApiConfig = node.gateway;
        points.push(
          new WebPoint(con.node, api.host, api.port, api.route, api.type, api.https, api.noWebSocket, api.header)
        );
      }
      return points;
    } catch (e: any) {
      const error: Error = e as Error;
      throw new SAMConfigError(`Failed to retrieve Bridge WebPoints of node "${nodeName}"! ${error.message}`);
    }
  }

  getWebEndpoints(nodeName: string): WebPoint[] {
    try {
      const node: SAMNode = this.config.nodes[nodeName];
      this.checkConfigNode(nodeName, node);
      const points: WebPoint[] = [];
      if (!node.endpoints) {
        return points;
      }
      for (const end of node.endpoints) {
        if (end.type === 'WebApi') {
          try {
            const api: MLSApiConfig = end.dataSource as MLSApiConfig;
            this.checkSAMApiConfig(api, 'Endpoint');
            points.push(
              new WebPoint(end.name, api.host, api.port, api.route, api.type, api.https, api.noWebSocket, api.header)
            );
          } catch (e: any) {
            const error: Error = e as Error;
            throw new SAMConfigError(`Endpoint "${end.name}" is no valid WebApi! ${error.message}`);
          }
        }
      }
      return points;
    } catch (e: any) {
      const error: Error = e as Error;
      throw new SAMConfigError(`Failed to retrieve Endpoint WebPoint of node "${nodeName}"! ${error.message}`);
    }
  }

  getConnectedNodes(nodeName: string): string[] {
    const pointNames: string[] = [];
    const node: SAMNode = this.config.nodes[nodeName];
    if (!node) {
      throw new Error(`Failed to retrieve Endpoints! Node name "${nodeName}" does not exist in SAM config file!`);
    }
    if (node.bridges) {
      node.bridges.forEach(b => pointNames.push(b.node));
    }
    // if (node.endpoints) {
    //   node.endpoints.forEach(e => pointNames.push(e.name));
    // }
    return pointNames;
  }

  // setupDataSources(nodeName: string): Map<string, DataSourceHandler> {
  //   try {
  //     const node: SAMNode = this.config.nodes[nodeName];
  //     this.checkConfigNode(nodeName, node);
  //     const dataSources: Map<string, DataSourceHandler> = new Map<string, DataSourceHandler>();
  //     if (!node.endpoints) {
  //       return dataSources;
  //     }
  //     for (const end of node.endpoints) {
  //       if (end.type === 'WebApi') {
  //         const sourceConfig: SAMApiConfig = end.dataSource as SAMApiConfig;
  //         if (sourceConfig.type === 'GQL') {
  //           dataSources.set(end.name, this.setupGQLDataSource());
  //         }
  //       } else if (end.type === 'Database') {
  //
  //       }
  //     }
  //     return dataSources;
  //   } catch (e: any) {
  //     const error: Error = e as Error;
  //     throw new SAMConfigError(`Failed to setup DataSources of node "${nodeName}"! ${error.message}`);
  //   }
  // }

  protected setupGQLDataSource(): any {

  }

  protected createDatabase(): any {

  }

  protected toBoolPromise(obs: Observable<ExecutionState>): Promise<boolean> {
    return this.toResolvedPromise(obs.pipe(map(execState => this.execStateToBool(execState))));
  }

  protected toResolvedPromise(obs: Observable<any>): Promise<any> {
    return obs.pipe(take(1)).toPromise();
  }

  protected execStateToBool(execState: ExecutionState): boolean {
    switch (execState) {
      case 'LOCAL-EXECUTION':
      case 'REMOTE-EXECUTION':
      case 'TEMP-LOCAL-EXECUTION':
        return true;
      case 'CONNECTION-FAILURE':
      case 'PROGRAM-FAILURE':
      default:
        return false;
    }
  }

  protected getGQLSchemaString(doc: DocumentNode): string {
    return doc.loc && doc.loc.source.body;
  }

  protected checkConfigClientNode(name: string, node: SAMNode): void {
    this.checkConfigNode(name, node);
    if ((!node.bridges || node.bridges.length === 0) && !node.gateway) {
      console.warn(`There are no Bridge WebPoints and no Gateway for node "${name}" defined, thus resulting in an isolated node!`);
    }
  }

  protected checkConfigServerNode(name: string, node: SAMNode, portNeeded: boolean = true): void {
    this.checkConfigNode(name, node);
    this.checkConfigGateway(node, portNeeded);
  }

  protected checkConfigGateway(node: SAMNode, portNeeded: boolean = true): void {
    if (!node.gateway) {
      throw new SAMConfigError('Gateway is not defined!');
    }
    if (portNeeded && !node.gateway.port) {
      throw new SAMConfigError('Port is not defined on Gateway!');
    }
    this.checkSAMApiConfig(node.gateway, 'Gateway');
  }

  protected checkSAMApiConfig(api: MLSApiConfig, type: 'Gateway' | 'Endpoint'): void {
    if (!api.host || !api.route) {
      throw new SAMConfigError(`Host or Route of ${type} is not defined!`);
    }
    if (api.type !== 'GQL' && api.type !== 'REST') {
      throw new SAMConfigError(`Api Type of ${type} is neither "GQL" nor "REST"!`);
    }
  }

  protected checkConfigNode(name: string, node: SAMNode): void {
    if (!node) {
      throw new SAMConfigError(`Node name "${name}" does not exist in the NodeList of SAM config file!`);
    }
  }
}

export interface ApolloGQLConfig {
  typeDefs: Array<DocumentNode>;
  resolvers: Resolvers<SAMGQLContext>;
  subscriptions: Partial<any>;
  // context: ContextFunction<ExpressContext, Context>;
}

export class WebPoint {
  constructor(
    public name: string,
    public host: string,
    public port: number,
    public uri: string,
    public type: RouteType,
    public https: boolean = false,
    public noWs: boolean = false,
    public headers?: RequestHeaders
  ) {}
}

export class ServerAPIConfig {
  constructor(
    public https: boolean,
    public host: string,
    public route: string,
    public type: RouteType,
    public port?: number
  ) {}
}

export class SAMConfigError extends Error {}

export type RouteType = 'GQL' | 'REST' | string;
