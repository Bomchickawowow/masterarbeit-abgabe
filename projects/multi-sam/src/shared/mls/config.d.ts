export interface MultiSAMConfig {
  default: SAMConfig;
  nodes: NodeList;
}

export interface SAMConfig {
  store: StoreConfig;
  archive: ArchiveConfig;
}

export type NodeList = {[key: string]: SAMNode};

export interface SAMNode extends SAMConfig {
  endpoints?: DataEndpoint[];
  bridges?: Bridge[];
  gateway?: MLSApiConfig;
}

export interface StoreConfig {
  maxArrayLength?: number;
}

export interface ArchiveConfig {
  maxRecentEntries?: number;
}

// export type EndPointList = {[key: string]: Endpoint};

export interface DataEndpoint {
  name: string;
  type: EndpointType | string;
  dataSource: MLSApiConfig | DatabaseConfig;
}

export type EndpointType = 'Database' | 'WebApi';

export interface DatabaseConfig {
  type: string;
}

export interface Bridge {
  node: string;
  apiRoute: string;
}

export interface MLSApiConfig {
  https: boolean;
  host: string;
  port?: number;
  route: string;
  type: ApiType | string;
  header?: RequestHeaders;
  noWebSocket?: boolean;
  externalApi?: boolean;
}

export type ApiType = 'GQL' | 'REST';

export type RequestHeaders = {[key: string]: string};

export type FilePath = string;

