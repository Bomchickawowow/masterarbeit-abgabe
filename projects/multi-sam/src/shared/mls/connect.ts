import { HttpLink } from 'apollo-angular/http';
import { ApolloClient, ApolloLink, Operation, split, createHttpLink } from '@apollo/client/core';
import { RequestClient } from './request-client';
import { SchemaLink } from '@apollo/client/link/schema';
import { Logger } from '../util/logger';
import { WebSocketLink } from '@apollo/client/link/ws';
import { GraphQLSchema, OperationDefinitionNode } from 'graphql';
import { getMainDefinition } from '@apollo/client/utilities';
import { ErrorResponse, onError } from '@apollo/client/link/error';
import { RequestHeaders } from './config';
import { HttpHeaders } from '@angular/common/http';
import { ConnectionParamsOptions } from 'subscriptions-transport-ws/dist/client';
import { ClientOptions, SubscriptionClient } from 'subscriptions-transport-ws';
import { ApolloGQLConfig, Configurator, WebPoint } from './configurator';
import { RepresentationBorder } from './representation-border';

export class Connect {
  constructor(
    private nodeName: string,
    private apollo: ApolloClient<any>,
    private configurator: Configurator,
    private rb: RepresentationBorder,
    private rs: RequestClient,
    private httpLink?: HttpLink
  ) {
    this.initEndpointConnections();
    this.initRBConnection();
    this.rs.initUpdateObserver();
  }

  isServer(): boolean {
    return typeof window === 'undefined';
  }

  protected initEndpointConnections(): void {
    const endPoints: WebPoint[] = this.configurator.getWebEndpoints(this.nodeName);
    const links: ApolloLink = this.setupEndpointLinks(endPoints);
    this.bindEndpointLinks(this.apollo, links);
  }

  protected initRBConnection(): void {
    const bridges: WebPoint[] = this.configurator.getBridgePoints(this.nodeName);
    const links: ApolloLink = this.setupRequestStationLinks(bridges, this.rb.getSchema(), this.httpLink);
    this.bindRequestStationLinks(this.apollo, links);
  }

  protected setupRequestStationLinks(webPoints: WebPoint[], rbSchema?: GraphQLSchema, httpLink?: HttpLink): ApolloLink {
    const terminalLink: ApolloLink = this.setupRBLink(rbSchema);
    const webLink: ApolloLink = this.setupMultipleWebLinks(webPoints, httpLink);
    if (!webLink && !terminalLink) {
      return undefined;
    } else if (!webLink) {
      return terminalLink;
    } else if (!terminalLink) {
      return webLink;
    } else {
      return split(
        (operation) => {
          return operation.getContext().location === RequestClient.RS_TERMINAL_TAG;
        },
        terminalLink,
        webLink
      );
    }
  }

  protected setupEndpointLinks(endPoints: WebPoint[], httpLink?: HttpLink): any {
    return this.setupMultipleWebLinks(endPoints, httpLink);
  }

  protected bindTwoLinksTogether(first: ApolloLink, second: ApolloLink, firstSelectionRule: (op: Operation) => boolean): ApolloLink {
    return split(firstSelectionRule, first, second);
  }

  protected bindRequestStationLinks(apollo: ApolloClient<any>, rsLinks: ApolloLink): void {
    if (!rsLinks) {
      Logger.debug('No RequestClient links to bind!', this);
      return;
    }
    this.dynamicBindLinkToApolloClient(apollo, rsLinks, (op: Operation) => {
      return op.getContext().origin === RequestClient.RS_ORIGIN_TAG;
    });
  }

  protected bindEndpointLinks(apollo: ApolloClient<any>, endPointLink: ApolloLink): void {
    if (!endPointLink) {
      Logger.debug('No Endpoint Links to bind!', this);
      return;
    }
    this.dynamicBindLinkToApolloClient(apollo, endPointLink, (op: Operation) => {
      return !op.getContext().origin;
    });
  }

  protected dynamicBindLinkToApolloClient(apollo: ApolloClient<any>, link: ApolloLink, selectionRule: (op: Operation) => boolean): void {
    if (this.hasApolloClientLinks(apollo)) {
      this.bindLinkToApolloClient(apollo, link, selectionRule);
    } else {
      this.setLinkOfApolloClient(apollo, link);
    }
  }

  protected bindLinkToApolloClient(apollo: ApolloClient<any>, link: ApolloLink, selectionRule: (op: Operation) => boolean): void {
    apollo.setLink(split(
      selectionRule,
      link,
      apollo.link
    ));
  }

  protected setLinkOfApolloClient(apollo: ApolloClient<any>, link: ApolloLink): void {
    apollo.setLink(link);
  }

  protected hasApolloClientLinks(apollo: ApolloClient<any>): boolean {
    return !!apollo.link;
  }

  // TODO: Better Error Logging!!
  protected handleConnectionErrors(error: ErrorResponse, customMessage: string): void {
    console.error(customMessage);
    if (error.graphQLErrors) {
      for (const err of error.graphQLErrors) {
        // console.error(
        //   `[GraphQL error]: Message: "${err.message}", Location: "${JSON.stringify(err.locations)}", Extensions: "${JSON.stringify(err.extensions)}"`
        // );
        console.error(err);
      }
    } else {
      console.error(error);
    }
  }

  protected setupRBLink(schema: GraphQLSchema): SchemaLink {
    if (!schema) {
      return undefined;
    }
    return new SchemaLink({
      schema: schema
    });
  }

  protected setupMultipleWebLinks(webPoints: WebPoint[], link?: HttpLink): ApolloLink {
    if (!webPoints || webPoints.length === 0) {
      return undefined;
    }
    let webLink: ApolloLink = this.setupWebLink(webPoints[0], link);
    for (let i: number = 1; i < webPoints.length; i++) {
      const webLinkTwo: ApolloLink = this.setupWebLink(webPoints[i], link);
      webLink = split(
        (operation) => {
          return operation.getContext().location === webPoints[i].name;
        },
        webLinkTwo,
        webLink
      );
    }
    return webLink;
  }

  // TODO: Missing forwarding for endpoints of endpoints! For Example in the connection of client -> load-balancer -> server,
  //  client has to forward requests to load-balancer in order to reach server
  protected setupWebLink(webPoint: WebPoint, link?: HttpLink): ApolloLink {
    const protocol: string = (webPoint.https) ? 'https' : 'http';
    const uri: string = (webPoint.uri.charAt(0) === '/') ? webPoint.uri : ('/' + webPoint.uri);
    const port: string = (webPoint.port) ? (':' + webPoint.port) : '';
    const httpUri: string = `${protocol}://${webPoint.host}${port}${uri}`;
    if (webPoint.type === 'REST') {
      Logger.error(`Error for WebLink "${httpUri}": REST-Apis are not supported yet!`);
      return undefined;
    }
    const http: ApolloLink = this.createHttpLink(httpUri, webPoint.headers, link);
    const errorLink: ApolloLink = this.createErrorLink(`Error at WebServer "${webPoint.name}" with uri "${httpUri}":`);
    // TODO: Enable WebSocket Connections for Servers
    if (webPoint.noWs || this.isServer()) {
      return errorLink.concat(http);
    }
    const ws: WebSocketLink = this.createWebSocketLink(webPoint.host, port, uri, {clientId: this.configurator.getClientId()});
    return errorLink.concat(split(
      // split based on operation type
      (operation) => {
        const op: OperationDefinitionNode = getMainDefinition(operation.query) as OperationDefinitionNode;
        return op.kind === 'OperationDefinition' && op.operation === 'subscription';
      },
      ws,
      http
    ));
  }

  protected createErrorLink(customMessage: string): ApolloLink {
    return onError((error) => {
      this.handleConnectionErrors(error, customMessage);
      return undefined;
    });
  }

  protected createHttpLink(httpUri: string, headers?: RequestHeaders, link?: HttpLink): ApolloLink {
    Logger.debug(`Creating Http Connection to endpoint "${httpUri}"`, this);
    let httpHeaders: HttpHeaders = new HttpHeaders();
    let hasAuth: boolean = false;
    if (headers) {
      hasAuth = Object.keys(headers).map(k => k.toLowerCase()).findIndex(k => k === 'authorization') !== -1;
    } else {
      headers = {};
    }
    // TODO: Only add bearer token to internal node connections!
    if (!hasAuth) {
      headers['authorization'] = (this.configurator.getClientId()) ? `Bearer ${this.configurator.getClientId()}` : '';
    }
    Object.keys(headers).forEach(key => {
      httpHeaders = httpHeaders.set(key, headers[key]);
    });
    return (link) ? link.create({uri: httpUri, method: 'POST', headers: httpHeaders})
      : createHttpLink({uri: httpUri, headers: headers});
  }

  protected createWebSocketLink(host: string, port: string, uri: string, connectionParams: ConnectionParamsOptions): WebSocketLink {
    const webSocketConfig: ClientOptions = {
      reconnect: true,
      lazy: true,
      timeout: 20000,
      connectionParams: connectionParams
    };
    const wsUri: string = `ws://${host}${port}${uri}`;
    Logger.debug(`Creating WebSocket Connection to endpoint "${wsUri}"`, this);
    const subscriptionClient: SubscriptionClient = new SubscriptionClient(wsUri, webSocketConfig);
    const wsLink: WebSocketLink = new WebSocketLink(subscriptionClient);
    // window.addEventListener('beforeunload', () => {
    //   // tslint:disable-next-line:ban-ts-ignore
    //   // @ts-ignore - the function is private in typescript
    //   wsLink.subscriptionClient.close();
    // });
    return wsLink;
  }
}
