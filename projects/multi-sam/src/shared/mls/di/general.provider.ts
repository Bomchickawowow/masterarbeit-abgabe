import { RequestClient } from '../request-client';
import { DefaultOptions } from '@apollo/client/core';
import { Archive } from '../archive';
import { Store } from '../store';
import { ActionFactory } from '../action';
import 'cross-fetch';
import { Provider } from '@angular/core';


export function getGeneralProviders(nodeName: string, initModel: Object = {}): Provider[] {
  return [
    {provide: Archive, useClass: Archive},
    {
      provide: Store,
      useFactory: (archive: Archive, requestStation: RequestClient) => {
        return new Store(requestStation, archive, nodeName, initModel);
      },
      deps: [Archive, RequestClient]
    },
    {
      provide: ActionFactory,
      useFactory: () => {
        return new ActionFactory(nodeName);
      },
      deps: []
    }
  ];
}

export const apolloOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all'
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all'
  }
};
