import { MultiSAMConfig } from '../config';
import { Provider, Type } from '@angular/core';
import { apolloOptions, getGeneralProviders } from './general.provider';
import { ApolloGQLConfig, Configurator, WebPoint } from '../configurator';
import { Apollo, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { ApolloClient, ApolloLink, InMemoryCache } from '@apollo/client/core';
import { RequestClient } from '../request-client';
import { Dispatcher, Resourcer } from '../resolver';
import { Logger } from '../../util/logger';
import { Facade } from '../facade';
import { MLSModule } from '../mls-module';
import { RepresentationBorder } from '../representation-border';
import { Connect } from '../connect';

// TODO: Make Angular specific dependencies optional!
export function getClientProviders(config: MultiSAMConfig, nodeName: string, moduleProvs: Type<MLSModule>[],
                                   initModel: Object = {}): Provider[] {
  const c: Configurator = new Configurator(config);
  return [
    {
      provide: Configurator,
      useFactory: () => {
        return c;
      }
    },
    {
      provide: APOLLO_OPTIONS,
      useFactory: () => {
        return {
          ssrMode: true,
          cache: new InMemoryCache(),
          defaultOptions: apolloOptions,
          link: undefined
        };
      },
      deps: []
    },
    {
      provide: RequestClient,
      useFactory: (apollo: Apollo, configurator: Configurator) => {
        return new RequestClient(nodeName, configurator.getClientId(), apollo.client, configurator.getConnectedNodes(nodeName));
      },
      deps: [Apollo, Configurator]
    },
    {
      provide: Facade,
      useFactory: (apollo: Apollo, configurator: Configurator) => {
        const endPoints: WebPoint[] = configurator.getWebEndpoints(nodeName);
        return new Facade(configurator.setupWebDataSources(apollo.client, endPoints.map(e => e.name)));
      },
      deps: [Apollo, Configurator]
    },
    ...getGeneralProviders(nodeName, initModel),
    ...moduleProvs,
    {
      provide: RepresentationBorder,
      useFactory: (configurator: Configurator, ...modules: MLSModule[]) => {
        return new RepresentationBorder(configurator.getRepresentationBorderConfig(modules.map(m => m.resourcer)));
      },
      deps: [Configurator, ...moduleProvs]
    },
    {
      provide: Connect,
      useFactory: (httpLink: HttpLink, apollo: Apollo, rb: RepresentationBorder, configurator: Configurator, rs: RequestClient) => {
        return new Connect(nodeName, apollo.client, configurator, rb, rs);
      },
      deps: [HttpLink, Apollo, RepresentationBorder, Configurator, RequestClient]
    }
  ];
}
