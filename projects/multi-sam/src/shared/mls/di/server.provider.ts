import { Provider, Type } from '@angular/core';
import { ServerApi } from '../server-api';
import { Store } from '../store';
import { ApolloGQLConfig, Configurator, WebPoint } from '../configurator';
import { apolloOptions, getGeneralProviders } from './general.provider';
import { MultiSAMConfig } from '../config';
import { ApolloClient, ApolloLink, InMemoryCache } from '@apollo/client/core';
import { RequestClient } from '../request-client';
import { Logger } from '../../util/logger';
import { Facade } from '../facade';
import { RepresentationBorder } from '../representation-border';
import { Connect } from '../connect';
import { MLSModule } from '../mls-module';

export function getServerProviders(config: MultiSAMConfig, nodeName: string,
                                   moduleProvs: Type<MLSModule>[],
                                   initModel: Object = {}): Provider[] {
  const c: Configurator = new Configurator(config);
  return [
    {
      provide: Configurator,
      useFactory: () => {
        return c;
      }
    },
    {
      provide: ApolloClient,
      useFactory: () => {
        return new ApolloClient({
          cache: new InMemoryCache(),
          defaultOptions: apolloOptions,
          link: undefined
        });
      },
      deps: []
    },
    {
      provide: RequestClient,
      useFactory: (apollo: ApolloClient<any>, configurator: Configurator) => {
        return new RequestClient(nodeName, configurator.getClientId(), apollo, configurator.getConnectedNodes(nodeName));
      },
      deps: [ApolloClient, Configurator]
    },
    {
      provide: Facade,
      useFactory: (apollo: ApolloClient<any>, configurator: Configurator) => {
        const endPoints: WebPoint[] = configurator.getWebEndpoints(nodeName);
        return new Facade(configurator.setupWebDataSources(apollo, endPoints.map(e => e.name)));
      },
      deps: [ApolloClient, Configurator]
    },
    ...getGeneralProviders(nodeName, initModel),
    ...moduleProvs,
    {
      provide: ServerApi,
      useFactory: (store: Store, configur: Configurator, ...modules: MLSModule[]) => {
        return new ServerApi(store, configur.getGatewayConfig(nodeName), configur.getServerAPIConfig(modules.map(m => m.dispatcher)));
      },
      deps: [Store, Configurator, ...moduleProvs]
    },
    {
      provide: RepresentationBorder,
      useFactory: (configurator: Configurator, ...modules: MLSModule[]) => {
        return new RepresentationBorder(configurator.getRepresentationBorderConfig(modules.map(m => m.resourcer)));
      },
      deps: [Configurator, ...moduleProvs]
    },
    {
      provide: Connect,
      useFactory: (apollo: ApolloClient<any>, rb: RepresentationBorder, configurator: Configurator, rs: RequestClient) => {
        return new Connect(nodeName, apollo, configurator, rb, rs);
      },
      deps: [ApolloClient, RepresentationBorder, Configurator, RequestClient]
    }
  ];
}
