import { Query } from 'graphql/dist/model.types';
import { Action } from './action';
import produce, { Patch } from 'immer';
import { Selection } from './selection';
import { ObjectHelper } from '../util/object-helper';
import { DocumentNode } from 'graphql';

export type Model = Query;

export interface SubModel {
  schema?: DocumentNode;
  localModel?: any;
}

/**
 * ModelChange provides the data to change the model and archive the change.
 * The route property defines the route to the SubModel, where the change is happening.
 * The causing property provides the Action, that led to the change.
 * The patches property provides the actual changes in patch data.
 */
export interface ModelChange {
  route: Selection;
  causing: Action;
  patches: Patch[];
}

/**
 * Retrieve SubModel from given model! If route is empty or not defined, it returns the model.
 * If the the specified Model Route does not exist on given model object, it returns 'undefined'
 *
 * @param route The route to the SubModel
 * @param model The Model Object
 * @return The specified SubModel or 'undefined' when the SubModel does not exist
 */
export function getSubModel(route: Selection, model: Object): Object {
  if (!route || route.length === 0) {
    return model;
  }
  let subModel: Object = model;
  for (const r of route) {
    if (ObjectHelper.hasOwnProperty(subModel, r)) {
      subModel = subModel[r];
    } else {
      return undefined;
    }
  }
  return subModel;
}

/**
 * Retrieve information whether given Model Object has given routes or not.
 *
 * @param routes The Model Routes
 * @param model The Model Object
 */
export function hasModelGivenRoutes(routes: Selection[], model: Model): boolean {
  for (const route of routes) {
    const subModel: Object = getSubModel(route, model);
    if (!subModel) {
      return false;
    }
  }
  return true;
}

/**
 * Setting a new SubModel of given Model Object. If route is empty or not defined it does nothing.
 * If checkPath is 'true' (that is the default case) and the specified Model Route does not exist
 * on given model object, it also does nothing.
 *
 * @param route The route to the SubModel
 * @param model The Model Object
 * @param newSubModel The new SubModel Object
 * @param checkPath Flag to enable route checking of the model object. If it is set to 'true' (default case)
 *        and the model does not have the specified route it breaks the process
 */
export function setSubModel(route: Selection, model: Object, newSubModel: any, checkPath: boolean = true): void {
  if (!route || route.length === 0) {
    return;
  }
  let subModel: Object = model;
  for (let i: number = 0; i < route.length - 1; i++) {
    if (!checkPath || ObjectHelper.hasOwnProperty(subModel, route[i])) {
      // Create route, if it's not existing
      if (subModel[route[i]] === undefined) {
        subModel[route[i]] = {};
      }
      subModel = subModel[route[i]];
    } else {
      return;
    }
  }
  if (!checkPath || ObjectHelper.hasOwnProperty(subModel, route[route.length - 1])) {
    subModel[route[route.length - 1]] = newSubModel;
  }
}

/**
 * Sets given subModel Object into given (immutable) model Object and returns it as an Immutable.
 * If route is not provided it returns the subModel Object and does not alter the given model Object.
 *
 * @param model The given Model Object
 * @param subModel The given SubModel Object
 * @param route The optional route to set the subModel
 * @return An immutable object with the subModel integrated into the model object
 */
export function setImmutableModel(model: Object, subModel: Object, route?: Selection): Object {
  return produce(model, (draft: any) => {
    if (!route || route.length === 0) {
      return subModel as Model;
    } else {
      setSubModel(route, draft, subModel, false);
      return;
    }
  });
}

/**
 * Returns a new immutable model object reduced to the given model routes.
 * Every property of given model object will be reset (set to 'undefined') except the given model routes.
 * The given model object will not be altered this way.
 *
 * @param routes The Model Routes
 * @param model The Model Object
 * @return A new reduced and immutable model object
 */
export function reduceModelToRoutes(routes: Selection[], model: Model): Model {
  return produce(model, (draft: Model) => {
    // Remove every property value of draft
    for (const prop of Object.keys(draft)) {
      draft[prop] = undefined;
    }
    // Path only properties of routes to draft
    for (const route of routes) {
      for (const r of route) {
        draft[r] = model[r];
      }
    }
  });
}
