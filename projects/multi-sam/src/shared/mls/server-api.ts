import { Action, NodeTarget } from './action';
import { PubSub, withFilter } from 'graphql-subscriptions';
import { Logger } from '../util/logger';
import { ApolloGQLConfig, ServerAPIConfig } from './configurator';
import { ApolloServer, ApolloServerExpressConfig } from 'apollo-server-express';
import { ExecutionState, Store } from './store';
import { Express } from 'express';
import * as express from 'express';
import * as http from 'http';
import { Server as HttpServer } from 'http';
import { GraphQLScalarType, GraphQLSchema, Kind } from 'graphql';
import { ValueNode } from 'graphql/language/ast';
import { delegateToSchema } from '@graphql-tools/delegate';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { Path } from '../util/path';
import * as cors from 'cors';
import { CorsOptions } from 'cors';
import { SAMGQLContext } from './resolver';
import { ActionInput, QueryResolvers, Resolvers } from 'graphql/dist/model.types';


export class ServerApi {
  protected readonly MODEL_CHANNEL: string = 'MODEL_CHANGED';
  protected apiServer: Express;
  protected apiConfig: ServerAPIConfig;
  protected store: Store;
  protected pubSub: PubSub;
  protected apollo: ApolloServer;
  protected gqlConfig: ServerAPIGQLConfig;

  protected clientList: Map<string, ClientContext>;

  // private originWhiteList: string[] = [
  //   'http://localhost:4300/',
  //   'http://gitlab.wogra.com/'
  // ];

  private corsOption: CorsOptions = {
    origin: true,
    methods: 'GET,HEAD,POST',
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
    preflightContinue: false,
    optionsSuccessStatus: 200,
    maxAge: 600,
    credentials: false
  };

  constructor(store: Store, apiConfig: ServerAPIConfig, gqlConfig: ApolloGQLConfig) {
    Logger.info('ServerAPI is setting up Server and Routes', this);
    this.clientList = new Map<string, ClientContext>();
    this.apiServer = express();
    this.store = store;
    this.pubSub = new PubSub();
    this.store.onDistribution().subscribe(change => {
      this.pubSub.publish(this.MODEL_CHANNEL, {modelChanged: change});
    });
    if (!gqlConfig) {
      throw new Error('Error in ServerAPI initialisation! GraphGQL Config is undefined! This could be the result of missing injected Dispatcher!');
    }
    this.gqlConfig = this.createFinalGQLConfig(gqlConfig);
    this.apiConfig = apiConfig;
    this.initRoutes();
  }

  open(): void {
    if (!this.apiConfig.port) {
      throw new Error('No Port was given for Server API! Maybe the port is missing in the sam-config.json.');
    }
    let port: string = '' + this.apiConfig.port;
    this.apiServer.set('port', port);
    port = ':' + port;
    const host: string = this.apiConfig.host;
    const protocol: string = (this.apiConfig.https) ? 'https' : 'http';

    const httpServer: HttpServer = http.createServer(this.apiServer);
    this.apollo.installSubscriptionHandlers(httpServer);
    httpServer.listen(this.apiConfig.port, () => {
      Logger.info(`🚀 GQL Server ready at ${protocol}://${host}${port}${this.apollo.graphqlPath}`, this);
      Logger.info(`🚀 GQL Subscriptions ready at ws://${host}${port}${this.apollo.subscriptionsPath}`, this);
    });
  }

  protected initRoutes(): void {
    if (this.apiConfig.type === 'GQL') {
      this.initGQLRoute();
    } else if (this.apiConfig.type === 'REST') {
      this.initRESTRoutes();
    }
  }

  protected initGQLRoute(): void {
    this.apollo = new ApolloServer(this.gqlConfig);
    this.apollo.applyMiddleware({ app: this.apiServer, path: this.apiConfig.route, cors: this.corsOption});
  }

  protected initRESTRoutes(): void {
    const baseRoute: string = this.apiConfig.route;
    Logger.debug(`Setting REST-API route "${baseRoute}"`, this);
    this.apiServer.get(`${baseRoute}/**`, (req, res) => {
      res.send('REST API handling is not implemented yet!');
    });
  }

  protected receiveActions(action: ActionInput): boolean {
    Logger.debug(action, this);
    this.store.present(action as Action);
    return true;
  }

  protected createFinalGQLConfig(gqlConfig: ApolloGQLConfig): ServerAPIGQLConfig {
    const queryMiddleware: QueryResolvers = this.createQueryMiddleware(gqlConfig);
    const actionResolvers: Resolvers = this.getActionResolvers();
    const apiConfig: ServerAPIGQLConfig = gqlConfig as ApolloServerExpressConfig;

    apiConfig.resolvers = {
      Query: queryMiddleware,
      Mutation: {
        ...actionResolvers.Mutation,
        ...gqlConfig.resolvers.Mutation
      },
      Subscription: {
        ...actionResolvers.Subscription,
        ...gqlConfig.resolvers.Subscription
      }
    };
    apiConfig.subscriptions = {
      onConnect: (connectionParams: any, websocket) => {
        Logger.debug('ON CONNECT', this);
        Logger.debug(connectionParams, this);
        if (!connectionParams.clientId) {
          throw new Error('Client Id is missing!');
        }
        return {
          wsUserId: this.authenticateClient(connectionParams.clientId)
        };
      }
    };
    apiConfig.context = (ctx) => {
      return {
        userId: this.getAuthToken(ctx)
      };
    };
    return apiConfig;
  }

  // TODO: Rename to UserToken. Replace header token!
  protected getAuthToken(ctx: ExpressContext): string {
    let token: string;
    // Context is HTTP request
    if (ctx.req) {
      // Get authToken of HTTP Request Header and authenticate it
      token = ctx.req.headers.authorization;
      token = (token) ?  token.substr(7) : '';
      return this.authenticateClient(token);
    // Context is Websocket request
    } else if (ctx.connection) {
      // Get authenticated userId provided by onConnect-function of subscriptions in createFinalGQLConfig
      return ctx.connection.context.wsUserId;
    }
  }

  protected authenticateClient(clientId: string): string {
    return clientId;
  }

  /**
   * Create Query Middleware for graphQL to enable query configuration.
   * One configuration is to register clients for their requested subModels, so they can be updated in case of a related change
   * by the graphQL subscription.
   *
   * @param config: GQL Config of Apollo for defining schemes and resolvers
   */
  protected createQueryMiddleware(config: ApolloGQLConfig): QueryResolvers {
    const newResolver: QueryResolvers = {};
    const schema: GraphQLSchema = makeExecutableSchema({
      typeDefs: [...config.typeDefs],
      resolvers: config.resolvers
    });
    // TODO: more precise targeting of the model route! Resolver entry is not specific enough
    for (const res of Object.keys(config.resolvers.Query)) {
      newResolver[res] = (parent, args, context, info) => {
        this.registerClient(context.userId, [res]);
        const delegation: any = delegateToSchema({
          schema: schema,
          operation: 'query',
          fieldName: res,
          args: args,
          context: context,
          info: info
        });
        return delegation;
      };
    }
    return newResolver;
  }

  protected registerClient(clientId: string, route: string[]): void {
    let clientInfo: ClientContext;
    if (this.clientList.has(clientId)) {
      clientInfo = this.clientList.get(clientId);
    } else {
      Logger.debug('Create Client Registration for id:', this);
      Logger.debug(clientId, this);
      clientInfo = {
        subscribedModels: new Map<string[], void>()
      };
    }
    clientInfo.subscribedModels.set(route);
    this.clientList.set(clientId, clientInfo);
  }

  protected getActionResolvers(): Resolvers<SAMGQLContext> {
    return {
      Mutation: {
        sendAction: this.receiveActions
      },
      Subscription: {
        modelChanged: {
          subscribe: withFilter(
            (rootValue, args, context, info) => this.pubSub.asyncIterator([this.MODEL_CHANNEL]),
            (rootValue: any, args, context, info) => {
              Logger.debug(context.userId, this);
              if (!context.userId || !this.clientList.has(context.userId) || !rootValue.modelChanged) {
                return false;
              }
              const modelList: Map<string[], void> = this.clientList.get(context.userId).subscribedModels;
              const related: boolean = Path.areRelated(modelList.keys(), rootValue.modelChanged.route);
              return related;
            }
          )
        }
      }
    };
  }

  protected getDateTypeDef(): GraphQLScalarType {
    return new GraphQLScalarType({
      name: 'Date',
      description: 'Date custom scalar type',
      serialize(value: Date): number {
        return value.getTime(); // Convert outgoing Date to integer for JSON
      },
      parseValue(value: any): Date {
        return new Date(value); // Convert incoming integer to Date
      },
      parseLiteral(ast: ValueNode): number {
        if (ast.kind === Kind.INT) {
          return parseInt(ast.value, 10); // Convert hard-coded AST string to type expected by parseValue
        }
        return undefined; // Invalid hard-coded value (not an integer)
      }
    });
  }
}

export type ServerAPIGQLConfig = ApolloGQLConfig | ApolloServerExpressConfig;

export interface ActionContext extends SAMGQLContext {
  from: NodeTarget;
  to: NodeTarget;
  created: number;
}

export interface ClientContext {
  subscribedModels: Map<string[], void>;
}

