import { ApolloClient } from '@apollo/client/core';
import { GQLObject } from './selection';
import { Action, NodeTarget } from './action';
import { ModelChanged } from './gql/root.query.gql';
import { Logger } from '../util/logger';
import { bindCallback, from, Observable, Observer } from 'rxjs';
import { ModelChangedSubscription } from 'graphql/dist/model.types';
import { ModelChange } from './model';

export class RequestClient {
  public static readonly RS_ORIGIN_TAG: string = 'RS';
  public static readonly RS_TERMINAL_TAG: string = '__TERMINAL__';
  public static readonly RS_BRIDGE_TAG: string = '__BRIDGE__';
  protected apollo: ApolloClient<any>;
  protected updateObserver: Observer<ModelChange>;
  protected readonly nodeName: string;
  protected readonly connectedNodes: Map<string, void>;
  protected readonly id: string;

  constructor(nodeName: string, clientID: string, apollo: ApolloClient<any>, connectedNodes: string[]) {
    this.id = clientID;
    this.apollo = apollo;
    this.nodeName = nodeName;
    this.connectedNodes = new Map<string, void>();
    connectedNodes.forEach(p => {
      if (this.connectedNodes.has(p)) {
        throw new Error(`Error in initialising Request Station! Duplicated ConnectionPoint Identifier "${p}" in SAM-Config for Node "${nodeName}"!`);
      }
      this.connectedNodes.set(p);
    });
  }

  getStationID(): string {
    return this.id;
  }

  setApollo(apollo: ApolloClient<any>): void {
    this.apollo = apollo;
  }

  // TODO: Missing the checking of endpoints of endpoints!
  hasConnectionPoint(name: string): boolean {
    return this.connectedNodes.has(name);
  }

  getModelFromTerminal(query: GQLObject): Observable<any> {
    const retrieveModelObs: (l: NodeTarget, q: GQLObject) => Observable<any> = bindCallback(this.sendStateQuery);
    return retrieveModelObs.call(this, undefined, query);
  }

  getModelFromBridge(location: NodeTarget, query: GQLObject): Observable<any> {
    const retrieveModelObs: (l: NodeTarget, q: GQLObject) => Observable<any> = bindCallback(this.sendStateQuery);
    return retrieveModelObs.call(this, location, query);
  }

  getRemoteModelChange(): Observable<ModelChange> {
    return new Observable<ModelChange>(sub => {
      this.updateObserver = sub;
    });
  }

  sendActionToBridge(location: NodeTarget, action: Action, mutation: GQLObject): Observable<void> {
    const sendActionObs: (l: NodeTarget, a: Action, m: GQLObject) => Observable<void> = bindCallback(this.sendActionMutation);
    return sendActionObs.call(this, location, action, mutation);
  }

  sendActionToTerminal(action: Action, mutation: GQLObject): Observable<any> {
    const sendActionObs: (l: NodeTarget, a: Action, m: GQLObject) => Observable<void> = bindCallback(this.sendActionMutation);
    return sendActionObs.call(this, undefined, action, mutation);
  }

  initUpdateObserver(): void {
    this.sendModelChangeSubscription((change) => {
      this.updateObserver.next(change);
    });
  }

  protected sendStateQuery2(query: GQLObject, location?: NodeTarget): Promise<any> {
    const context: any = {};
    context.origin = RequestClient.RS_ORIGIN_TAG;
    if (location) {
      context.location = location;
    } else {
      context.location = RequestClient.RS_TERMINAL_TAG;
    }
    return this.apollo.query({
      query: query,
      context: context
    });
  }

  protected sendActionMutation(location: NodeTarget, action: Action, mutation: GQLObject, callback: (data: any) => void): void {
    Logger.debug(`Send Action Mutation for action "${action.type}"!`, this);
    const context: any = {
      id: this.id,
      from: action.from,
      created: action.created,
      origin: RequestClient.RS_ORIGIN_TAG
    };
    if (location) {
      context.location = location;
    } else {
      context.location = RequestClient.RS_TERMINAL_TAG;
    }
    this.apollo.mutate({
      mutation: mutation,
      context: context,
      variables: action.payload
    }).then(success => {
      Logger.debug('sendAction finished!', this);
      if (!success) {
        Logger.error('Mutation send failed!', this);
      }
      callback(success);
    });
  }

  protected sendStateQuery(location: NodeTarget, query: GQLObject, callback: (data: any) => void): void {
    const context: any = {};
    context.origin = RequestClient.RS_ORIGIN_TAG;
    if (location) {
      context.location = location;
    } else {
      context.location = RequestClient.RS_TERMINAL_TAG;
    }
    this.apollo.query({
      query: query,
      context: context
    }).then(data => {
      callback(data.data);
    }).catch(err => {
      Logger.error(err, this);
    });
  }

  protected sendModelChangeSubscription(callback: (change: ModelChange) => void): void {
    this.apollo.subscribe({
      query: ModelChanged,
      context: {
        origin: RequestClient.RS_ORIGIN_TAG
      }
    }).subscribe(data => {
      Logger.debug('Got remote Model Change from Server!', this);
      const changeSub: ModelChangedSubscription = data.data;
      callback(changeSub.modelChanged as ModelChange);
    });
  }

}

