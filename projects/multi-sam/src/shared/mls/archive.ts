import { Action, ActionResult } from './action';
import { Observable, Subject } from 'rxjs';
import { Copy } from '../util/copy';
import { applyPatches, Patch } from 'immer';

export class Archive {
  protected static readonly MAX_HISTORY_SIZE: number = 6;
  protected static readonly NO_TIME_TRAVEL: number = Archive.MAX_HISTORY_SIZE + 5;

  protected pastActions: Action[] = [];
  protected pastChanges: Patch[][] = [];
  protected pastInverseChanges: Patch[][] = [];
  protected history: Subject<History>;
  protected timeTravel: Subject<Object>;
  // timeTravelMarker values are in: [0, MAX_HISTORY_SIZE] U {NO_TIME_TRAVEL}
  protected timeTravelMarker: number = Archive.NO_TIME_TRAVEL;
  protected currentState: Object;
  protected totalActionCount: number = 0;

  constructor() {
    this.history = new Subject<History>();
    this.timeTravel = new Subject<Object>();
  }

  // MAKE ACTION HISTORY AVAILABLE FOR TIME TRAVEL COMPONENT
  getHistory(): Observable<History> {
    return this.history.asObservable();
  }

  // SET TIME-TRAVEL STATE THROUGH HISTORY INDEX
  setPastState(historyIndex: number): void {
    if (historyIndex >= this.pastChanges.length) {
      throw new Error(`Cannot get past State! Given History Index of ${historyIndex} is larger than or equal the history itself (>= ${this.pastChanges.length})!`);
    }
    let newState: Object;
    if (historyIndex < this.timeTravelMarker) {
      newState = this.undo(historyIndex);
    } else {
      newState = this.redo(historyIndex);
    }
    this.timeTravelMarker = historyIndex;
    this.timeTravel.next(Copy.trueDeep(newState));
  }

  setCurrentState(state: Object): void {
    this.currentState = state;
  }

  // AFTER REDUCER APPLIED CHANGES FROM ACTION: ADD ENTRY IN HISTORY
  addEntry(actResult: ActionResult): void {
    // IF STATE HAS BEEN TIME-TRAVELED AND NEW MUTATION WAS DONE: DELETE OLD "FUTURE" ENTRIES
    if (this.timeTravelMarker !== Archive.NO_TIME_TRAVEL) {
      this.removeDeprecatedFuture();
    }
    if (this.pastChanges.length >= Archive.MAX_HISTORY_SIZE) {
      this.deleteOldestEntries();
    }
    this.pastActions.push(Copy.trueDeep(actResult.action) as Action);
    this.pastChanges.push(Copy.trueDeep(actResult.changes) as Patch[]);
    this.pastInverseChanges.push(Copy.trueDeep(actResult.inverseChanges) as Patch[]);
    this.totalActionCount ++;
    this.history.next({recentActions: this.pastActions, totalActionCount: this.totalActionCount});
  }

  // MAKE TIME-TRAVEL STATE FOR THE STORE AVAILABLE
  getTimeTravelState(): Observable<Object> {
    return this.timeTravel.asObservable();
  }

  protected undo(historyIndex: number): Object {
    const to: number = (this.timeTravelMarker === Archive.NO_TIME_TRAVEL) ? this.pastInverseChanges.length : this.timeTravelMarker;
    const changes: Patch[] = this.getPatchSequence(this.pastInverseChanges, historyIndex + 1, to + 1, true);
    return applyPatches(this.currentState, changes);
  }

  protected redo(historyIndex: number): Object {
    const changes: Patch[] = this.getPatchSequence(this.pastChanges, this.timeTravelMarker + 1, historyIndex + 1, false);
    return applyPatches(this.currentState, changes);
  }

  protected getPatchSequence(history: Patch[][], from: number, to: number, reversed: boolean = false): Patch[] {
    return history.slice(from, to).reduce((acc, curr) => {
      if (acc) {
        return (reversed) ? curr.concat(acc) : acc.concat(curr);
      }
      return curr;
    });
  }

  protected removeDeprecatedFuture(): void {
    this.pastChanges.splice(this.timeTravelMarker + 1);
    this.pastInverseChanges.splice(this.timeTravelMarker + 1);
    this.pastActions.splice(this.timeTravelMarker + 1);
    this.timeTravelMarker = Archive.NO_TIME_TRAVEL;
  }

  protected deleteOldestEntries(): void {
    let count: number = this.pastChanges.length + 1 - Archive.MAX_HISTORY_SIZE;
    for (count; count > 0; count--) {
      this.pastChanges.shift();
      this.pastInverseChanges.shift();
      this.pastActions.shift();
    }
  }
}

export interface History {
  recentActions: Action[];
  totalActionCount: number;
}



