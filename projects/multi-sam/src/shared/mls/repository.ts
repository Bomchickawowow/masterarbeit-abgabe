import Dexie from 'dexie';

export class Repository {
  db: any;

  constructor(config: RepositoryConfig) {
  }

  protected initDatabase(config: RepositoryConfig): void {
    switch (config.dbType) {
      case 'dexie':
      default:
        // this.db = new Dexie();
    }
  }
}

export interface RepositoryConfig {
  dbType: DB_TYPE;
  dbName: string;
  dbUser: string;
  dbPassword: string;
}

export type DB_TYPE = 'dexie';
