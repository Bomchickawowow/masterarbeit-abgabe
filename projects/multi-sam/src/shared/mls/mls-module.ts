import { SelectionBundle } from './selection';
import { Action, ActionBundle, ActionFactory } from './action';
import { Dispatcher, Resourcer } from './resolver';
import { Store } from './store';

/**
 * MLSModule is the collection of ModelReferences and ActonSpecs.
 * Its purpose is to encapsulate semantically the required data and data mutations (i. e. Selections and Actions) for a given Module.
 * Through this encapsulation the module's dispatcher can easily delegate the store to retrieve the required data and register the actions.
 * After this initialization the module has all preparation met to provide its service.
 */
export class MLSModule<T extends Action = Action> {
  constructor(
    public selections: SelectionBundle,
    public  actions: ActionBundle<T>,
    public  dispatcher: Dispatcher,
    protected  store: Store,
    public  resourcer?: Resourcer
  ) {}

  protected register(): void {
    this.store.registerModule(this);
  }
}

