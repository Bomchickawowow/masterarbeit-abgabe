import { DocumentNode } from 'graphql';
import { Patch } from 'immer';
import { Logger } from '../util/logger';
import { Action, NodeTarget } from './action';

/**
 * SelectionBundle is the bundle of different Selection.
 * Its purpose is to encapsulate semantically different Model Data for a given Module.
 */
export interface SelectionBundle {
  specs: SelectionSpec[];
}

/**
 * Selection is a reference object to the location, where given SubModel data is obtainable.
 * The location property provides the node name, where the model can be found. If omitted, the model is locally available
 * The route property defines the model route to the exact SubModel memory location regarding the stores model.
 * It is needed to either filter relevant changes regarding a subModel or to integrate a subModel into the model object!
 * The query property provides the necessary data to retrieve the remote model and is only needed, if the model data lays remotely
 * on another node in the network. If a query is given, it should be checked whether the query root path matches the route property
 * of the source object!
 */
export interface SelectionSpec {
  node?: NodeTarget;
  selection: Selection;
  query?: GQLObject;
}


/**
 * SelectionRoute represent the order of the selector path to access the SubModel in the store
 * Example: Access for model.shop.sales would be ['shop', 'sales']
 */
export type Selection = string[];

export type GQLObject = DocumentNode;

