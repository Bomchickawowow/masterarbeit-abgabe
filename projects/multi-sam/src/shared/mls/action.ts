import { Patch } from 'immer';
import { GQLObject, SelectionBundle } from './selection';
import { Model } from './model';

/**
 * An Action object represents a proposal to change the Model.
 * Its purpose is to deliver the need information to the reducer to change the model and to track
 * all app mutation throughout the lifetime of the app.
 */
export interface Action {
  // Meta-Information
  created: Date;
  from: string;

  // Action relevant information
  type: ActionType;
  payload: Object;
}

/**
 * An ActionResult object represents the model changes done by an action.
 * Its purpose is to deliver historical information to the archive, so changes can be tracked
 * and, when necessary, merged or reverted
 */
export interface ActionResult {
  action: Action;
  changes: Patch[];
  inverseChanges: Patch[];
  executedOn: NodeTarget;
}


/**
 * ActionBundle is the bundle of different ActionCollection.
 * Its purpose is to encapsulate semantically different ActionSpecs for a given Module.
 */
export interface ActionBundle<T extends Action = Action> {
  collections: ActionCollection<T>[];
}

/**
 * ActionCollection is the collection of different ActionSpecs
 * Its purpose is to encapsulate different ActionSpecs with the same ExecutionTarget.
 * Thereby the executionTarget property provide the node name, where given actions has to be executed.
 * If omitted, the actions have to be executed locally.
 */
export interface ActionCollection<T extends Action = Action> {
  executionTarget?: NodeTarget;
  specs: ActionSpec<T>[];
}

/**
 * ActionSpec defines a action specification regarding a unique action type in order to register given ActionType in the store.
 * The type property provides the unique action type name.
 * The mutation property is optional and provides the graphQL mutation object needed to forward the action to a remote node in the network.
 * If the executionTarget of parent ActionCollection IS NOT the current node, this property has to be set!
 * The reducer property is optional and provide the reducer function, which defines the execution logic behind the action.
 * If the executionTarget of parent ActionCollection IS the current node, this property has to be set!
 */
export interface ActionSpec<T extends Action = Action> {
  type: T['type'];
  mutation?: GQLObject;
  reducer?: Reducer<T['payload']>;
}

/**
 * ActionType defines the type of the action. It is used to identify which action belongs to which reducer
 */
export type ActionType = string;

/**
 * Reducers are functions, which define how the Model should be mutated (similar to redux)
 */
export type Reducer<T = any> = (model: Model, payload: T, terminalData: any) => void;

/**
 * A NodeTarget is the name of a node in the network. Its mostly used to identify the node with the action reducer execution right.
 */
export type NodeTarget = string | 'self';

// TODO: Create through code generation a string enum based on the nodes and endpoints of the system! Apply this enum to the NodeTarget type
//  to enforce type checking on real NodeTargets!

/**
 * ActionFactory is the factory class for action. It adds automatically meta information to the Action Object
 */
export class ActionFactory {
  constructor(
    private source: string
  ) {}

  create<T extends Action>(type: T['type'], data: T['payload']): Action {
    return {
      created: new Date(),
      from: this.source,
      type: type,
      payload: data
    };
  }
}

/**
 * Merges two Action Collections together.
 * Same-type ActionSpecs of the new Collection overwrites all properties of ActionSpec from old Collection.
 *
 * @param oldColl The old Action Collection
 * @param newColl The new Action Collection
 */
export function mergeActionCollections(oldColl: ActionCollection, newColl: ActionCollection): ActionCollection {
  const result: ActionCollection = {
    executionTarget: (newColl.executionTarget) ? newColl.executionTarget : oldColl.executionTarget,
    specs: oldColl.specs.slice(0, oldColl.specs.length)
  };
  for (const spec of newColl.specs) {
    const place: number = result.specs.findIndex(el => el.type === spec.type);
    if (place <= -1) {
      result.specs.push(spec);
    } else {
      const spliced: ActionSpec[] = result.specs.splice(place, 1);
      result.specs.push({
        type: spec.type,
        mutation: spec.mutation || spliced[0].mutation,
        reducer: spec.reducer || spliced[0].reducer
      });
    }
  }
  return result;
}

