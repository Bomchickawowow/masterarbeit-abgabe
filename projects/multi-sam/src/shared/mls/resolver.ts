import { ExecutionState, Store } from './store';
import { ActionBundle, ActionFactory } from './action';
import { DocumentNode, GraphQLResolveInfo } from 'graphql';
import { Observable } from 'rxjs';
import { Facade } from './facade';
import { SelectionBundle } from './selection';

// TODO: MLSResolver are grouping ModelData regarding a Service- / Modules purpose. Maybe rename?
export interface MLSResolver {
  schema: DocumentNode;
  resolution: GQLResolution;
}

export interface GQLResolution {
  Query: {[key: string]: (parent?: any, args?: any, context?: SAMGQLContext, info?: GraphQLResolveInfo) => Observable<any>};
  Mutation: {[key: string]: (parent?: any, args?: any, context?: SAMGQLContext, info?: GraphQLResolveInfo) => Observable<any>};
}

export interface SAMGQLContext {
  userId: string;
}

// TODO: Check needed whether given Dispatcher-schema has corresponding Transformer, if DataSource is at a Resourcer
//      Maybe simplified usage and definition of Dispatcher and Transformer for developer?

// TODO: Dispatcher also forwarding Query / Model Requests. Rename!
export class Dispatcher implements MLSResolver {
  schema: DocumentNode;
  resolution: GQLResolution;

  constructor(
    protected store: Store,
    protected af: ActionFactory,
    protected selections: SelectionBundle,
    protected actions: ActionBundle
  ) {}
}


export class Resourcer implements MLSResolver {
  schema: DocumentNode;
  resolution: GQLResolution;

  constructor(
    protected facade: Facade
  ) {}
}


// TODO: Define SAM Module, which bundles the module specific dispenser, terminal, transformer, datasource, reducer, schema and SamBundle-Config!
