import { concat, Observable, of, Subject } from 'rxjs';
import { Action, ActionBundle, ActionSpec, ActionType, NodeTarget, Reducer } from './action';
import { MLSModule } from './mls-module';
import { Archive } from './archive';
import { catchError, concatMap, filter, map } from 'rxjs/operators';
import { Path } from '../util/path';
import produce, { applyPatches, enablePatches, Patch, produceWithPatches } from 'immer';
import { SelectionSpec, Selection, GQLObject, SelectionBundle } from './selection';
import { Logger } from '../util/logger';
import { RequestClient } from './request-client';
import { OperationDefinitionNode } from 'graphql';
import { Model, ModelChange, getSubModel, reduceModelToRoutes, setImmutableModel } from './model';
import { Resourcer } from './resolver';

export class Store {
  protected readonly name: NodeTarget;
  protected model: Object;
  protected stateSubject: Subject<StateEmission>;
  protected changeSubject: Subject<ModelChange>;
  // TODO: unify all action related Maps
  protected reducers: Map<ActionType, Reducer>;
  protected mutations: Map<ActionType, GQLObject>;
  protected actionTargets: Map<ActionType, NodeTarget>;
  protected modelSources: Map<string[], SelectionSpec>;
  protected bundleRecords: Map<string, void>;
  protected archive: Archive;
  protected requestStation: RequestClient;
  protected readonly rbQueryRoutes: Map<string, void>;
  protected readonly rbMutationRoutes: Map<string, void>;

  constructor(client: RequestClient, archive: Archive, name: NodeTarget, initialModel?: Object) {
    this.name = name;
    this.model = produce(initialModel, draft => (draft) ? draft : {});
    this.stateSubject = new Subject<StateEmission>();
    this.changeSubject = new Subject<ModelChange>();
    // this.modelRefs = new Map<ActionType, SelectionRoute[]>();
    this.reducers = new Map<ActionType, Reducer>();
    this.mutations = new Map<ActionType, GQLObject>();
    this.actionTargets = new Map<ActionType, NodeTarget>();
    this.modelSources = new Map<string[], SelectionSpec>();
    this.bundleRecords = new Map<string, void>();
    this.rbQueryRoutes = new Map<string, void>();
    this.rbMutationRoutes = new Map<string, void>();
    enablePatches();
    this.bindArchive(archive);
    this.bindRequestStation(client);
    this.updateState();
  }

  // TODO: Better naming convention of Model and State!
  /**
   * Retrieve system state from the store's model referenced by given SelectionBundle!
   * If model information is not locally present, a query will be fired to retrieve data from remote location (i. e. node of the network).
   * The resulting observable delivers the requested state as an immutable copy of the stores model and prospective updates related to it.
   *
   * @param from The SelectionBundle containing the references to the needed subModels of the system state.
   * @return An observable of the immutable copy of the store's model, reduced to the referenced subModels.
   *         It also delivers updates related to the referenced subModels.
   */
  getState(from: Selection[]): Observable<Model> {
    // RETRIEVE MODEL SOURCES FROM GIVEN MODEL ROUTES
    // TODO: Missing Check whether given modelRoutes are valid
    const sources: SelectionSpec[] = from.map(r => this.modelSources.get(r)).filter(s => !!s);
    // RESOLVE MODEL REFERENCES TO GET ALL NEEDED MODEL DATA
    const reducedModelObs: Observable<Model> = this.resolveModelReferences(sources);
    // RETURN CONCAT OBSERVABLE FROM RESOLVED MODEL REFERENCES AND STATE SUBJECT
    return concat(reducedModelObs, this.stateSubject
      // FILTER CHANGES RELATED TO THE SAME MODEL BRANCH OF STATE MODEL
      .pipe(filter(em => {
        return Path.areRelated(sources.map(ref => ref.selection), em.route);
      }))
      // REDUCE STATE MODEL OBJECT TO GIVEN MODEL ROUTES
      .pipe(map(e => {
        return reduceModelToRoutes(sources.map(ref => ref.selection), e.state);
      })));
  }

  /**
   * Present an action to the store in order to change the system state!
   * The action will either be executed right away or will be forwarded to its execution target (i. e. a node in the network)
   *
   * @param action The Action object
   * @return An observable of the ExecutionState regarding which execution process got taken.
   */
  present(action: Action): Observable<Execution> {
    // FIND EXECUTION TARGET OF ACTION TYPE
    const execTarget: string = this.actionTargets.get(action.type);
    // IF EXECUTION TARGET IS NOT THIS NODE, FORWARD ACTION
    if (execTarget !== 'self' && execTarget !== this.name) {
      return this.forwardActionToNode(execTarget, action);
    }
    // IF EXECUTION TARGET IS THIS NODE, BUT NO REDUCER WAS FOUND, THROW ERROR!
    if (!this.reducers.has(action.type)) {
      throw new Error(`Execution target is this node, but no Reducer found for action type "${action.type}"!`);
    }
    let execution: Observable<any> = of({});
    // CHECK TERMINAL ROUTES AND FIRE REQUESTS ON THEM
    if (this.mutations.has(action.type) && this.hasMatchingRBRoutes(this.mutations.get(action.type))) {
      execution = this.forwardActionToTerminal(action);
    }
    // EXECUTE ACTION WITH OPTIONAL TERMINAL DATA
    return execution.pipe(map(terminalData => {
      Logger.debug(`Process action "${action.type}"!`, this);
      // APPLY REDUCER CHANGES AND PATCHES TO TEMPORAL VARIABLE
      const changedModel: [Model, Patch[], Patch[]] = this.executeAction(action, terminalData);
      // APPLY CHANGES
      this.applyChanges(changedModel, action);
      // RETURN LOCAL-EXECUTION AS EXECUTION-STATE TO INFORM CALLER
      return {data: undefined, state: 'LOCAL-EXECUTION'};
    }));
  }

  /**
   * Registers a SAMBundle to the store in order to deliver mandatory meta information of given ActionTypes!
   * The registered SAMBundle name will be saved in an records dictionary to prevent duplicated registration.
   * After the registration process the stores knows, what to do with incoming actions of given ActionTypes.
   *
   * @param module The SAMBundle, containing meta information about the bundle's models and actions.
   */
  registerModule(module: MLSModule): void {
    // IF SAM-BUNDLE HAS BEEN ALREADY REGISTERED, DO NOTHING
    if (this.bundleRecords.has(module.constructor.name)) {
      return;
    } else {
      // ELSE SAVE SAM-BUNDLE RECORD AND REGISTER EVERY ACTION-SPEC AND MODEL SOURCE
      this.bundleRecords.set(module.constructor.name);
      if (!module.actions || !module.actions.collections) {
        return;
      }
      try {
        for (const family of module.actions.collections) {
          this.checkNodeTarget(module.constructor.name, family.executionTarget);
          for (const spec of family.specs) {
            if (family.executionTarget) {
              this.registerAction(spec, family.executionTarget);
            } else {
              this.registerAction(spec);
            }
          }
        }
      } catch (e) {
        Logger.error(`MLSModule "${module.constructor.name}" has action type conflict with itself or other bundle:`, this);
        Logger.error(e, this);
        return;
      }
      try {
        for (const source of module.selections.specs) {
          this.checkNodeTarget(module.constructor.name, source.node);
          this.registerModelSource(source);
        }
      } catch (e) {
        Logger.error(`MLSModule "${module.constructor.name}" has ModelSource conflict with itself or other bundle:`, this);
        Logger.error(e, this);
        return;
      }
      try {
        if (module.resourcer) {
          this.setRBRoutes(module.resourcer);
        }
      } catch (e) {
        Logger.error(e, this);
        return;
      }
    }
  }

  /**
   * Enables subscription to the store's model changes.
   * This function's purpose is to enable the delivery of model changes to the Gateway,
   * so model changes can be passed down to underlying nodes of the network.
   */
  onDistribution(): Observable<ModelChange> {
    return this.changeSubject.asObservable();
  }

  protected hasMatchingRBRoutes(gql: GQLObject): boolean {
    Logger.debug('Check Route for RepresentationBorder!', this);
    const node: any = gql.definitions[0] as OperationDefinitionNode;
    for (const field of node.selectionSet.selections) {
      if (node.operation === 'query') {
        if (!this.rbQueryRoutes.has(field.name.value)) {
          return false;
        }
      } else if (node.operation === 'mutation') {
        if (!this.rbMutationRoutes.has(field.name.value)) {
          return false;
        }
      } else {
        Logger.debug('GQL Object is neither "query" nor "mutation"', this);
        return false;
      }
    }
    Logger.debug('It has terminal Route!', this);
    return true;
  }


  protected setRBRoutes(resourcer: Resourcer): void {
    if (!resourcer) {
      return;
    }
    for (const key of Object.keys(resourcer.resolution.Query)) {
      this.rbQueryRoutes.set(key);
    }
    for (const key of Object.keys(resourcer.resolution.Mutation)) {
      this.rbMutationRoutes.set(key);
    }
  }

  /**
   * Registers an ActionSpec to the store by checking constraints and adding the ActionType MetaInformation to corresponding dictionaries.
   * After Registration the store knows, what to do with incoming actions of the registered action type.
   *
   * @param spec The Action Specification of given ActionType
   * @param executionTarget The target node for executing given action type
   */
  protected registerAction(spec: ActionSpec, executionTarget: NodeTarget = 'self'): void {
    Logger.debug(`Register Action "${spec.type}"!`, this);
    if (this.actionTargets.has(spec.type) && this.actionTargets.get(spec.type) !== executionTarget) {
      throw new Error(`There is already the execution target "${this.actionTargets.get(spec.type)}" defined for action "${spec.type}"
      and it differs from the given one of "${executionTarget}"!`);
    }
    this.actionTargets.set(spec.type, executionTarget);
    if (spec.reducer) {
      if (this.reducers.has(spec.type)) {
        throw new Error(`There is already a reducer defined for action type "${spec.type}"!`);
      }
      this.reducers.set(spec.type, spec.reducer);
    }
    if (executionTarget === 'self') {
      if (!spec.reducer) {
        throw new Error(`Execution target of Action "${spec.type}" is "self" but Reducer of Action is missing!`);
      }
      return;
    }
    if (!spec.mutation) {
      throw new Error(`Execution target of Action "${spec.type}" is not "self" but GraphQL Mutation is missing!`);
    }
    if (this.mutations.has(spec.type)) {
      throw new Error(`There is already a graphql mutation defined for action type "${spec.type}"!`);
    }
    this.mutations.set(spec.type, spec.mutation);
  }


  /**
   * Register a Selection to the store by checking constraints and adding the Selection MetaInformation to the modelSource dictionary.
   * After the Registration the store knows, what to do with incoming Model Requests of given ModelRoutes.
   *
   * @param source The Selection to be registered
   */
  protected registerModelSource(source: SelectionSpec): void {
    const modelRoute: string = Path.toString(source.selection);
    Logger.debug(`Register ModelSource "${modelRoute}"`, this);
    if (this.modelSources.has(source.selection) && this.modelSources.get(source.selection) !== source) {
      throw new Error(`There is already a ModelSource defined for Model Route "${modelRoute}" and it differs from the given one!`);
    }
    const subRoute: string[] = [];
    for (const edge of source.selection) {
      subRoute.push(edge);
      if (subRoute.length !== source.selection.length && this.modelSources.has(subRoute)) {
        throw new Error(`There is already a ModelSource defined for subRoute "${Path.toString(subRoute)}"
         of given ModelRoute "${modelRoute}", overshadowing the given ModelRoute!`);
      }
    }
    if (source.query) {
      this.checkQueryPath(source.selection, source.query);
    }
    this.modelSources.set(source.selection, source);
  }

  /**
   * Binds Archive to the Store. Archive receive every state update and store subscribes to time-travel function
   * in order to undo / redo previous states.
   *
   * @param archive The Archive
   */
  protected bindArchive(archive: Archive): void {
    Logger.debug('Store is binding Archive!', this);
    this.archive = archive;
    this.stateSubject.asObservable().pipe(map(em => em.state)).subscribe(state => {
      this.archive.setCurrentState(state);
    });
    // SUBSCRIBE TO TIME-TRAVEL STATE AND SET MODEL TO SET STATE
    this.archive.getTimeTravelState().subscribe(model => {
      // TODO: How does time-travel interact with remote changes ?
      this.model = setImmutableModel(this.model, model);
      this.updateState();
    });
  }

  /**
   * Binds RequestClient to the Store. Store subscribes to remote model changes, received by the request station.
   *
   * @param requestStation The RequestClient
   */
  protected bindRequestStation(requestStation: RequestClient): void {
    Logger.debug('Store is binding RequestClient!', this);
    this.requestStation = requestStation;
    this.requestStation.getRemoteModelChange().subscribe(change => {
      this.applyRemoteChange(change);
    });
  }

  /**
   * Resolves the given ModelSources and returns an Observable with the Model Object REDUCED to the model routes of the model sources.
   * Therefore the returned model Object only has the properties related to the given Model Sources.
   * If the referenced SubModels cannot be find in this store, they will be retrieved from given source location.
   *
   * @param sources The Model Sources. Defines where to get remote Models and how to reduce the Store Model
   * @return An observable of the reduced model defined by the given ModelSources
   */
  protected resolveModelReferences(sources: SelectionSpec[]): Observable<Model> {
    // TODO: Clearer Error Messages!
    if (!sources || sources.length <= 0) {
      throw new Error('Given ModelSources for State-Observable is empty or not defined!');
    }
    let obs: Observable<Model> = of(setImmutableModel({}, {}) as Model);
    for (const source of sources) {
      if (!source.selection || source.selection.length <= 0) {
        throw new Error('Given route of Selection for State-Observable is empty or not defined!');
      }
      // CHECK IF MODEL HAS REQUESTED SUBMODEL
      const subModel: Object = getSubModel(source.selection, this.model);
      if (subModel) {
        obs = obs.pipe(map(m => setImmutableModel(m, subModel, source.selection) as Model));
      } else {
        // IF MODEL DOESNT HAVE SUBMODEL BUT HAS SOURCE INFORMATION, RETRIEVE SUBMODEL FROM SOURCE
        obs = obs.pipe(concatMap((m) => this.retrieveExternalModel(source)
          .pipe(map(d => setImmutableModel(m, d, source.selection) as Model))
        ));
      }
    }
    return obs;
  }

  protected retrieveExternalModel(source: SelectionSpec): Observable<Model> {
    try {
      if (!source.query || !source.node) {
        throw new Error('Selection does not present a valid source: query, node or gql property are missing!');
      }
      if (source.node !== 'self' && source.node !== this.name) {
        return this.retrieveModelFromBridge(source);
      } else {
        return this.retrieveModelFromTerminal(source);
      }
      // throw new Error('Selection does not present a valid source: Source location is itself, but both model property and terminal resolver is missing!');
    } catch (e: any) {
      const error: Error = e as Error;
      throw new Error(`Error at ModelSource route "model.${Path.toString(source.selection)}"! ${error.message}`);
    }
  }

  protected retrieveModelFromTerminal(source: SelectionSpec): Observable<Model> {
    Logger.debug('Request Model from Resourcer!', this);
    return this.requestStation.getModelFromTerminal(source.query).pipe(map(data => {
      Logger.debug('Received Model from Resourcer!', this);
      const d: any = getSubModel(source.selection, data);
      this.model = setImmutableModel(this.model, d, source.selection);
      return d;
    }));
  }

  /**
   * Retrieves remote subModel referenced by given Selection.
   * By receiving remote subModel, it will be saved into this stores model and then the stores model will be returned.
   *
   * @param source The Selection of remote subModel
   * @return An observable of the stores model merged with the retrieved subModel
   */
  protected retrieveModelFromBridge(source: SelectionSpec): Observable<Model> {
    // TODO: Missing Error Case if data is null!
    return this.requestStation.getModelFromBridge(source.node, source.query).pipe(map(data => {
      const d: any = getSubModel(source.selection, data);
      this.model = setImmutableModel(this.model, d, source.selection);
      return d;
    }));
  }

  /**
   * Forward action to remote node for execution.
   * If forwarding does not work (because of network problems) and the reducers logic is locally available,
   * execute action temporary locally.
   *
   * @param action The action
   * @return An observable of the ExecutionState regarding which execution process got taken
   */
  protected forwardActionToNode(execTarget: NodeTarget, action: Action): Observable<Execution> {
    if (!this.mutations.has(action.type)) {
      throw new Error(`No Mutation defined for action type "${action.type}" although action execution target is not this node!`);
    }
    Logger.debug(`Forward action "${action.type}"!`, this);
    return this.requestStation.sendActionToBridge(execTarget, action, this.mutations.get(action.type))
      .pipe(map(() => 'REMOTE-EXECUTION'))
      .pipe(catchError<any, Observable<ExecutionState>>((err, caught) => {
        if (this.reducers.has(action.type)) {
          return this.temporaryLocalExecution(action);
        } else {
          return of('CONNECTION-FAILURE');
        }
      }));
  }

  protected forwardActionToTerminal(action: Action): Observable<any> {
    if (!this.mutations.has(action.type)) {
      // TODO: Better Error message
      throw new Error(`No Mutation defined for action type "${action.type}" although action execution target is not this node!`);
    }
    Logger.debug(`Forward action "${action.type}"!`, this);
    return this.requestStation.sendActionToTerminal(action, this.mutations.get(action.type)).pipe(map(d => {
      if (d.data) {
        return d.data;
      } else {
        return undefined;
      }
    })).pipe(catchError((err, caught) => {
      Logger.debug(err, this);
      return undefined;
    }));
      // .pipe(map(() => 'REMOTE-EXECUTION'))
      // .pipe(catchError<any, Observable<ExecutionState>>((err, caught) => {
      //   if (this.reducers.has(action.type)) {
      //     return this.temporaryLocalExecution(action);
      //   } else {
      //     return of('CONNECTION-FAILURE');
      //   }
      // }));
  }

  protected temporaryLocalExecution(action: Action): Observable<ExecutionState> {
    // TODO: Implement!
    return of('TEMP-LOCAL-EXECUTION');
  }

  /**
   * Execute reducer on draft version of state through library 'Immer'.
   *
   * @param action An action Object
   * @param terminalData Optional data from executing action mutation on the terminal
   * @return A Tuple with the changed State, the changes and the inverseChanges
   */
  protected executeAction(action: Action, terminalData: any): [Model, Patch[], Patch[]] {
    return produceWithPatches((draft: Model, act: Action, externalData: any) => {
      this.reducers.get(act.type)(draft, act.payload, externalData);
    })(this.model as Model, action, terminalData);
  }

  /**
   * Applies remote changes to current stores model through 'Immer'!
   *
   * @param change The ModelChange object, received from remote location
   */
  protected applyRemoteChange(change: ModelChange): void {
    const changedModel: [Model, Patch[], Patch[]] = produceWithPatches((draft, patches) => {
      applyPatches(draft, patches);
    })(this.model as Model, change.patches);
    this.applyChanges(changedModel, change.causing);
  }

  /**
   * Applies changed model draft to current store model, fills changes, distributes changes and updates state!
   *
   * @param changedModel The changed model draft, used to apply the changes
   * @param action The corresponding action resulting in this change
   */
  protected applyChanges(changedModel: [Model, Patch[], Patch[]], action: Action): void {
    // ACCEPT CHANGE FOR MODEL
    this.acceptChanges(changedModel[0]);
    // ADD HISTORY ENTRY IN ARCHIVE
    this.filing(action, changedModel[1], changedModel[2]);
    // GET MODELROUTE FOR FILTERING UPDATES
    const route: string[] = Path.longestCommonPath(changedModel[1].map(patch => patch.path));
    // DISTRIBUTE MODEL CHANGES TO SUBSCRIBED SUB-NODES IN NETWORK
    this.distribute({route: route, causing: action, patches: changedModel[1]});
    // UPDATE LOCAL STATE WITH CHANGES
    this.updateState(route);
  }

  protected acceptChanges(newModel: Model): void {
      this.model = newModel;
  }

  protected storeChanges(): void {
    // TODO: Implement!
  }

  protected filing(action: Action, relChanges: Patch[], relInverseChanges: Patch[], executedOn?: NodeTarget): void {
    if (this.archive) {
      this.archive.addEntry({
        action: action,
        changes: relChanges,
        inverseChanges: relInverseChanges,
        executedOn: (executedOn) ? executedOn : this.requestStation.getStationID()
      });
    }
  }

  protected distribute(change: ModelChange): void {
    this.changeSubject.next(change);
  }

  protected updateState(route?: Selection): void {
    if (!route) {
      this.stateSubject.next({route: [], state: this.model as Model});
      return;
    }
    this.stateSubject.next({route: (route) ? route : [], state: this.model as Model});
  }

  protected checkNodeTarget(bundleName: string, nodeName: string): void {
    if (nodeName && nodeName !== 'self' && nodeName !== this.name && !this.requestStation.hasConnectionPoint(nodeName)) {
      throw new Error(`The given node target "${nodeName}" of SAM Bundle "${bundleName}"
            is not reachable from current node "${this.name}"! Either the node does not exist or there is no connection to it!`);
    }
  }

  protected checkQueryPath(modelRoute: Selection, query: GQLObject): void {
    Logger.debug(`Check Query Path for route "${Path.toString(modelRoute)}"!`, this);
    let node: any = query.definitions[0] as OperationDefinitionNode;
    let queryPath: string = 'Query';
    for (const part of modelRoute) {
      if (!node.selectionSet || !node.selectionSet.selections || node.selectionSet.selections.length === 0) {
        throw new Error(`Given ModelRoute "${Path.toString(modelRoute)}" is longer than given QueryPath "${queryPath}"!`);
      }
      node = node.selectionSet.selections.find(n => n.name && n.name.value === part);
      if (!node) {
        throw new Error(`Given ModelRoute "${Path.toString(modelRoute)}" does not match any SubPath after given QueryPath "${queryPath}"!`);
      }
      queryPath += '.' + node.name.value;
    }
  }
}

export interface StateEmission {
  route: Selection;
  state: Model;
}

export type Execution = {data: any, state: ExecutionState};
export type ExecutionState = 'LOCAL-EXECUTION' | 'REMOTE-EXECUTION' | 'CONNECTION-FAILURE' | 'PROGRAM-FAILURE' | 'TEMP-LOCAL-EXECUTION';
