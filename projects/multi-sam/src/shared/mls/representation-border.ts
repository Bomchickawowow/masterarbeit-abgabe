import { ApolloGQLConfig } from './configurator';
import { GraphQLSchema } from 'graphql';
import { makeExecutableSchema } from '@graphql-tools/schema';

export class RepresentationBorder {
  private schema: GraphQLSchema;

  constructor(config: ApolloGQLConfig) {
    if (!config) {
      return;
    }
    this.schema = makeExecutableSchema(config);
  }

  getSchema(): GraphQLSchema {
    return this.schema;
  }
}
