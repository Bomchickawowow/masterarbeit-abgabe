import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeTravelComponent } from './time-travel/time-travel.component';
import { AppShellRenderDirective } from './app-shell/app-shell-render.directive';
import { AppShellNoRenderDirective } from './app-shell/app-shell-no-render.directive';


@NgModule({
  declarations: [
    AppShellRenderDirective,
    AppShellNoRenderDirective,
    TimeTravelComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TimeTravelComponent,
    AppShellRenderDirective,
    AppShellNoRenderDirective,
  ],
  providers: []
})
export class CoreModule {}
