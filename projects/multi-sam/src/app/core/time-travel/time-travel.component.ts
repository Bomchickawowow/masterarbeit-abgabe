import { Component, OnInit } from '@angular/core';
import { Action } from '../../../shared/mls/action';
import { Archive } from '../../../shared/mls/archive';

@Component({
  selector: 'app-time-travel',
  templateUrl: './time-travel.component.html',
  styleUrls: ['./time-travel.component.scss']
})
export class TimeTravelComponent {
  history: Action[] = [];
  totalActionCount: number = 0;
  indexActiveEntry: number = 0;

  constructor(
    private archive: Archive
  ) {
    this.archive.getHistory().subscribe(history => {
      this.history = history.recentActions;
      this.totalActionCount = history.totalActionCount;
      this.indexActiveEntry = this.history.length - 1;
    });
  }

  onEntryClick(index: number): void {
    this.archive.setPastState(index);
    this.indexActiveEntry = index;
  }

  prettyPrint(obj: any): string {
    return JSON.stringify(obj, undefined, 2);
  }

}
