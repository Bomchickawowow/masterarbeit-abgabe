import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TodoModule } from './todo/todo.module';
import { CoreModule } from './core/core.module';
import { HttpClientModule } from '@angular/common/http';
import Config from './../sam-config.json';
import { sharedModuleProvider, sharedProvider } from '../shared/mls-modules/shared.provider';
import { getClientProviders } from '../shared/mls/di/client.provider';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    CoreModule,
    HttpClientModule,
    TodoModule
  ],
  providers: [
    ...getClientProviders(Config, 'client', sharedModuleProvider),
    ...sharedProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
