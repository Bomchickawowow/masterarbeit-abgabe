import { Component, OnInit } from '@angular/core';
import { TodoState } from './todo.state';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { TodoDispatcher } from '../../shared/mls-modules/todo/todo.dispatcher';
import { Connect } from '../../shared/mls/connect';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  animations: [
    trigger('changePosition', [
      state('*', style({transform: 'translateY({{y}}px)'}), {params: {y: 0}}),
      transition(':enter', [
        style({opacity: 0, transform: 'none'}),
        animate('1ms',  style({transform: 'translateY({{y}}px)'})),
        animate('200ms ease-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({opacity: 0}))
      ]),
      transition(':increment, :decrement', animate('200ms', style({transform: 'translateY({{y}}px)'})))
    ])
  ]
})
export class TodoComponent implements OnInit {
  input: string = '';
  todo: TodoState;

  constructor(
    private todoResolver: TodoDispatcher,
    private connect: Connect
  ) {}

  ngOnInit(): void {
    this.todoResolver.getTodoAsLists().subscribe(todo => {
      this.todo = todo;
    });
  }

  // ===============================================================================
  // =====================> TECHNICAL EVENT TO SEMANTIC EVENT <=====================
  // ===============================================================================

  onInsertInputKeypress(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      const input: HTMLInputElement = event.target as HTMLInputElement;
      this.todoResolver.addTodoItem(input.value).subscribe();
      input.value = '';
    }
  }

  onDeleteClick(todo: string): void {
    this.todoResolver.removeTodoItem(todo).subscribe();
  }

  onCheckChange(todo: string, value: boolean): void {
    if (value) {
      this.todoResolver.finishTodoItem(todo).subscribe();
    } else {
      this.todoResolver.unfinishTodoItem(todo).subscribe();
    }
  }

}
