import { NgModule } from '@angular/core';
import { TodoComponent } from './todo.component';
import { TodoItemComponent } from './todo-item/todo-item.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [
    TodoComponent
  ],
  declarations: [
    TodoComponent,
    TodoItemComponent
  ],
  providers: []
})
export class TodoModule {}
