export interface TodoState {
  finishedTodos: string[];
  unfinishedTodos: string[];
}
