import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss'],
})
export class TodoItemComponent {
  @Input() todo: string = '';
  @Input() checked: boolean = false;
  @Output() readonly checkChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() readonly deleteClick: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  onCheckboxClick(): void {
    this.checkChanged.emit(!this.checked);
  }

  onDeleteButtonClick(): void {
    this.deleteClick.emit();
  }
}
