import 'zone.js/dist/zone-node';

import { Logger } from './src/shared/util/logger';
import { ServerApi } from './src/shared/mls/server-api';
import { getServerProviders } from './src/shared/mls/di/server.provider';
import { join } from 'path';
import { existsSync } from 'fs';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { AppServerModule } from './src/app/app.server.module';
import * as express from 'express';
import { APP_BASE_HREF } from '@angular/common';
import { Express } from 'express';
import { serverProvider } from './src/server/provider/server.provider';
import { ReflectiveInjector } from '@angular/core';
import Config from './src/sam-config.json';
import { sharedModuleProvider, sharedProvider } from './src/shared/mls-modules/shared.provider';


// The Express app is exported so that it can be used by serverless Functions.
export class AppServer {
  private readonly nodeName: string = 'server';
  private injector: ReflectiveInjector;
  private server: Express;
  private initModel: Object = {
    todo: {
      todoList: [
        {
          text: 'Making the laundry',
          done: false
        },
        {
          text: 'Vacuum cleaning my room',
          done: false
        },
        {
          text: 'Learning GraphQL',
          done: true
        }
      ]
    }
  };

  constructor(
    private readonly environment: SYSTEM_MODE = 'DEVELOPMENT'
  ) {
    Logger.initSource(this.nodeName);
    this.initServices();
    this.initSSRSetup();
  }

  start(): void {
    Logger.info('', this);
    Logger.info('------------------------------------', this);
    Logger.info('----------  SERVER START  ----------', this);
    Logger.info('------------------------------------', this);
    Logger.info('', this);

    const port: string = process.env.PORT;
    this.server.set('port', port);
    this.server.listen(port, () => {
      Logger.info(`🚀 Angular is ready at http://localhost:${port}`, this);
    });

    const gateway: ServerApi = this.injector.get(ServerApi) as ServerApi;
    gateway.open();
  }

  private initServices(): void {
    // TODO: DI not working on server after building for production!!
    this.injector = ReflectiveInjector.resolveAndCreate([
      ...getServerProviders(Config, this.nodeName, sharedModuleProvider, this.initModel),
      ...sharedProvider,
      ...serverProvider
    ]);
  }

  private initSSRSetup(): void {
    this.server = express();
    const distFolder: string = join(process.cwd(), 'dist/multi-sam/browser');
    const indexHtml: string = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

    // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
    this.server.engine('html', ngExpressEngine({
      bootstrap: AppServerModule
    }));

    this.server.set('view engine', 'html');
    this.server.set('views', distFolder);

    // Serve static files from /browser
    this.server.get('*.*', express.static(distFolder, {
      maxAge: '1y'
    }));

    // TODO: inject relative dependency here ?!
    // All regular routes use the Universal engine
    this.server.get('*', (req, res) => {
      res.render(indexHtml, {
        req,
        providers: [
          { provide: APP_BASE_HREF, useValue: req.baseUrl }
          // ...getDIProviders('client')
        ]
      });
    });
  }
}

export type SYSTEM_MODE = 'PRODUCTION' | 'DEVELOPMENT' | 'TESTING';




function run(): void {
  // const port: string = process.env.PORT || '4200';
  const server: AppServer = new AppServer();
  server.start();
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule: NodeModule = __non_webpack_require__.main;
const moduleFilename: string = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
